/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.File;
import java.io.IOException;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;

/**
 * The Class IndexOfHierarchyTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class IndexOfHierarchyTest
{
	private static Logger logger = LoggerFactory.getLogger(IndexOfHierarchyTest.class);

	/**
	 * Test index of hierarchy 02.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testIndexOfHierarchy02() throws IOException, FlatDB4GeoNamesException
	{
		Runtime.getRuntime().gc();
		long a = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Free   memory= " + Runtime.getRuntime().freeMemory() / 1024 / 1024);
		System.out.println("Max    memory= " + Runtime.getRuntime().maxMemory() / 1024 / 1024);
		System.out.println("Total  memory= " + Runtime.getRuntime().totalMemory() / 1024 / 1024);
		System.out.println("Used   memory= " + a / 1024 / 1024);

		IndexOfHierarchy index = new IndexOfHierarchy(new File("data/hierarchy.txt"));

		Runtime.getRuntime().gc();
		long b = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Free   memory= " + Runtime.getRuntime().freeMemory() / 1024 / 1024);
		System.out.println("Max    memory= " + Runtime.getRuntime().maxMemory() / 1024 / 1024);
		System.out.println("Total  memory= " + Runtime.getRuntime().totalMemory() / 1024 / 1024);
		System.out.println("Used   memory= " + b / 1024 / 1024);
		System.out.println("Diff   memory= " + (a - b) / 1024 / 1024);

		System.out.println("count=" + index.size());

		Assertions.assertThat(index.get(6454879)).isEqualTo(6457367);
	}
}
