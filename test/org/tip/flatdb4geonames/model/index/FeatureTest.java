/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.IOException;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;

/**
 * The Class FeatureTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class FeatureTest
{
	private static Logger logger = LoggerFactory.getLogger(FeatureTest.class);

	/**
	 * Test feature 01.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testFeature01() throws IOException, FlatDB4GeoNamesException
	{
		Assertions.assertThat(Feature.convertToCodes(null)).isNull();
		Assertions.assertThat(Feature.convertToCodes("H").getLeft()).isEqualTo("H");
		Assertions.assertThat(Feature.convertToCodes("H").getRight()).isNull();
		Assertions.assertThat(Feature.convertToCodes("H.").getLeft()).isEqualTo("H");
		Assertions.assertThat(Feature.convertToCodes("H").getRight()).isNull();
		Assertions.assertThat(Feature.convertToCodes("H.CNLA").getLeft()).isEqualTo("H");
		Assertions.assertThat(Feature.convertToCodes("H.CNLA").getRight()).isEqualTo("CNLA");
	}
}
