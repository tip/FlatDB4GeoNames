/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import java.io.IOException;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.index.FeatureClass;
import org.tip.flatdb4geonames.util.Chronometer;

/**
 * The Class FlatDB4GeoNamesTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class FlatDB4GeoNamesTest
{
	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNamesTest.class);

	public static final String DATABASE_HOME = "data/";

	/**
	 * Test 0.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void test0() throws IOException, FlatDB4GeoNamesException
	{
		FlatDB4GeoNames.open(DATABASE_HOME);

		String result = FlatDB4GeoNames.instance().searchFeatureDescriptionPath("US.NC.157");

		System.out.println("=>" + result);
	}

	/**
	 * Test 1.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	// @Test
	// public void testGEDLinePattern01() throws Exception
	// {
	//
	// logger.debug("========================== testGEDLinePattern01");
	//
	// Matcher matcher = GEDFile.GEDLINE_PATTERN.matcher("0 HEAD");
	//
	// System.out.println("find=" + matcher.find());
	// System.out.println("count=" + matcher.groupCount());
	//
	// Assertions.assertThat(matcher.find()).isTrue();
	// Assertions.assertThat(matcher.groupCount()).equals(12);
	// }

	/**
	 * @throws IOException
	 * @throws FlatDB4GeoNamesException
	 */
	@Test
	public void test1() throws IOException, FlatDB4GeoNamesException
	{
		FlatDB4GeoNames.open(DATABASE_HOME);

		GeoNamesLines lines = FlatDB4GeoNames.instance().search("Massy", FeatureClass.CITY_VILLAGE);

		Assertions.assertThat(lines).isNotNull();
		Assertions.assertThat(lines.containsGeoNameId(2995206)).isTrue();
		Assertions.assertThat(lines.containsGeoNameId(3002895)).isTrue();
	}

	/**
	 * Test 2.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void test2() throws IOException, FlatDB4GeoNamesException
	{
		FlatDB4GeoNames.open(DATABASE_HOME);

		// GeoNamesLines lines =
		// FlatDB4GeoNames.instance().searchAdministrativePathToponym(tag)

		// Assertions.assertThat(lines).isNotNull();
		// Assertions.assertThat(lines.containsGeoNameId(2995206)).isTrue();
		// Assertions.assertThat(lines.containsGeoNameId(3002895)).isTrue();
	}

	/**
	 * Test 9.
	 */
	@Test
	public void test9()
	{
		try
		{
			// FlatDB4GeoNames.buildIndex(new File("data/"));
			// System.exit(0);

			FlatDB4GeoNames.open("data/");

			// for (int index = 165000; index < 165310; index++)
			// {
			// String line =
			// FlatDB4GeoNames.instance().getIndexWordSeeks().getIndex2Line(index);
			// System.out.println(index + "=[" + line + "] " + line.length() +
			// " " + line.getBytes().length);
			// }
			// System.out.println("-------");
			// {
			// String line =
			// FlatDB4GeoNames.instance().getIndexWordSeeks().getIndex2Line(3071831);
			// System.out.println(3071831 + "=[" + line + "] " + line.length() +
			// " " + line.getBytes().length);
			// }
			// System.out.println("-------");
			Chronometer chrono = new Chronometer();
			// StringList lines = FlatDB4GeoNames.instance().getLine("ﾎﾛﾑｲ");
			// StringList lines = FlatDB4GeoNames.instance().getLine("aa");
			// StringList lines =
			// FlatDB4GeoNames.instance().getLine("paspossible");
			// System.out.println("Time:" + chrono.stop().interval());
			// for (String line : lines)
			// {
			// System.out.println("MASSY: " + line);
			// }

			GeoNamesLines lines = FlatDB4GeoNames.instance().search("Massy", FeatureClass.CITY_VILLAGE);
			for (GeoNamesLine line : lines)
			{
				System.out.println(line.toLine());
			}

			// for (String line :
			// FlatDB4GeoNames.instance().searchWord("massy"))
			// {
			// System.out.println("MASSY: " + line);
			// }

			/*
			FlatDB4GeoNames.instance().searchRawLineByGeoNameId(3038823);

			FlatDB4GeoNames.open("data/");
			System.out.println(FlatDB4GeoNames.instance().searchRawLineByGeoNameId(3038823));
			System.out.println(FlatDB4GeoNames.instance().searchRawLineByGeoNameId(6454879));
			System.out.println(FlatDB4GeoNames.instance().searchRawLineByGeoNameId(2));
			System.out.println(FlatDB4GeoNames.instance().searchRawLineByGeoNameId(10300939));
			System.out.println(FlatDB4GeoNames.instance().searchRawLineByGeoNameId(10299919));

			System.out.println(FlatDB4GeoNames.instance().searchRawLineByWord("Massy"));
			*/
			// IndexOfIdSeek.buildIndex(new File("data/allCountries.txt"), new
			// File("output/id_seek.padded"), 2000000, 23);

			// IndexOfIdSeek.geonamesToIdSeek(new File("output/test9.source"),
			// new File("output/test9-id_seek"));
			//
			// StringFileSorter.sortBigStringFile(new
			// File("output/test9-id_seek"), new
			// File("output/test9-id_seek.sorted"), 2000000, new
			// StringLineIdComparator());

		}
		catch (IOException exception)
		{
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		catch (FlatDB4GeoNamesException exception)
		{
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
	}

	/**
	 * Test flat DB 4 geo names 00.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testFlatDB4GeoNames00() throws IOException, FlatDB4GeoNamesException
	{
		Runtime.getRuntime().gc();
		long a = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Used   memory= " + a / 1024 / 1024);

		FlatDB4GeoNames.open(DATABASE_HOME);

		Runtime.getRuntime().gc();
		long b = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Used   memory= " + b / 1024 / 1024);

		FlatDB4GeoNames.instance().close();

		Runtime.getRuntime().gc();
		long c = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Used   memory= " + c / 1024 / 1024);
	}
}
