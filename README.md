  
# FlatDB4GeoNames

GeoNames is geographical database under a creative commons attribution license.

FlatDB4GeoNames provides an easy, local and fast Java access to GeoNames data.

## Author
Christian Pierre MOMON &lt;christian.momon@devinsy.fr&gt;

## License
This software is released under the GNU AGPL license.

## Requirements

FlatDB4Geonames requires:
- Java 1.6
- Eclipse Kepler 

## Context
GeoNames is geographical database under a creative commons attribution license.

It is strongly recommended to support the project: <a href="http://www.geonames.org/">http://www.geonames.org/</a>

There is many ways to use GeoNames:

* official webservices but:
  * you need a Internet connexion
  * these webservices are not Free Sotfware
  * queries are slow
  * free query count is limited
* dump GeoNames files into a SQL Database but you need to install and manage a SQL Dababase
* load GeoNames dump but it requires a huge memory (> 10GB)

FlatDB4GeoNames provides a solution using flat index files on GeoNames dump files:
* First, you download	the GeoNames dump files.
* Then, you build indexes.
* And finally, you query the indexes.

The FlatDB4GeoNames menu contains all these actions.

Figures about FlatDBGeoNames are:  
* 2 GB of disk space (1.2GB from GeoNames dump files + 0.8GB from indexes)
* small memory impact (<30MB)
* fast requests

## Download GeoNames files
GeoNames provides official dump files on <a href="http://download.geonames.org/export/dump/">http://download.geonames.org/export/dump/</a>.

FlatDB4GeoNames requires some of them. To download the required files, use <i>Menu > File > Download GeoNames files…</i>

## Build local indexes##
The main GeoNames file takes 1.2GB and contains more than 10 million of entries. To increase request performance,
FlatDB4GeoNames build indexes to reach faster GeoNames data by geonameId and words.

To build indexes, use <i>Menu > File > Build indexes…</i>

## Open database</h2>
Once GeoNames dump files are downloaded and once indexes are build, you can open a database.

A FlatDB4GeoNames database is defined by the directory which contains the GeoNames files and FlatDB4GeoNames indexes.

Once database opened, main tabs are available:
* general info
* query
* statistics

## Search

Search is using quick algorithm with complexity of O(log2(word_count)). Actually, word_count is around 6,000,000. So, it is really quick.

## Conclusion
Enjoy and use FlatDB4GeoNames. For questions, improvement, issues: christian.momon@devinsy.fr
	