#!/bin/sh
# Set the current directory with the FlatDB4GeoNames directory.
cd "`dirname "$0"`"

#
echo "Information about default installed Java:"
java -version

# Try to launch FlatDB4GeoNames with Java 6.
java -Xms368m -Xmx1024m -version:1.6* -jar flatdb4geonames.jar $@

# If failed, try with the default installed Java.
if [ $? -ne 0 ] ; then
	#
	echo "Launching with the default installed Java."
	java -Xms368m -Xmx1024m -jar flatdb4geonames.jar $@
fi
exit
