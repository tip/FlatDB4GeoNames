/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.tip.flatdb4geonames.util.StringLongPairComparator.Sorting;

/**
 * The Class StringLongPairs.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class StringLongPairs extends ArrayList<StringLongPair>
{
	private static final long serialVersionUID = 4878326812587289547L;

	/**
	 * Instantiates a new string long pairs.
	 */
	public StringLongPairs()
	{
		super();
	}

	/**
	 * Instantiates a new string long pairs.
	 * 
	 * @param capacity
	 *            the capacity
	 */
	public StringLongPairs(final int capacity)
	{
		super(capacity);
	}

	/**
	 * Contains string.
	 * 
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean containsString(final String target)
	{
		boolean result;

		if (getByString(target) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Gets the by string.
	 * 
	 * @param target
	 *            the target
	 * @return the by string
	 */
	public StringLongPair getByString(final String target)
	{
		StringLongPair result;

		boolean ended = false;
		Iterator<StringLongPair> iterator = this.iterator();
		result = null;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				StringLongPair current = iterator.next();

				if (StringUtils.equals(current.getString(), target))
				{
					ended = true;
					result = current;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * Removes the last.
	 * 
	 * @return the string long pair
	 */
	public StringLongPair removeLast()
	{
		StringLongPair result;

		if (size() > 0)
		{
			result = remove(size() - 1);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Reverse.
	 * 
	 * @return the string long pairs
	 */
	public StringLongPairs reverse()
	{
		StringLongPairs result;

		Collections.reverse(this);

		result = this;

		//
		return result;
	}

	/**
	 * Sort by long.
	 * 
	 * @return the string long pairs
	 */
	public StringLongPairs sortByLong()
	{
		StringLongPairs result;

		Collections.sort(this, new StringLongPairComparator(Sorting.LONG));

		result = this;

		//
		return result;
	}

	/**
	 * Sort by string.
	 * 
	 * @return the string long pairs
	 */
	public StringLongPairs sortByString()
	{
		StringLongPairs result;

		Collections.sort(this, new StringLongPairComparator(Sorting.STRING));

		result = this;

		//
		return result;
	}
}
