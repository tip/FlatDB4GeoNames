/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.util.Comparator;

/**
 * The Class StringLongPairComparator.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class StringLongPairComparator implements Comparator<StringLongPair>
{
	public enum Sorting
	{
		STRING,
		LONG
	}

	private Sorting sorting;

	/**
	 * Instantiates a new string long pair comparator.
	 * 
	 * @param sorting
	 *            the sorting
	 */
	public StringLongPairComparator(final Sorting sorting)
	{
		this.sorting = sorting;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final StringLongPair alpha, final StringLongPair bravo)
	{
		int result;

		//
		result = compare(alpha, bravo, this.sorting);

		//
		return result;
	}

	/**
	 * Compare.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 * @param sorting
	 *            the sorting
	 * @return the int
	 */
	public static int compare(final StringLongPair alpha, final StringLongPair bravo, final Sorting sorting)
	{
		int result;

		//
		if (sorting == null)
		{
			result = 0;
		}
		else
		{
			//
			switch (sorting)
			{
				case STRING:
				{
					String limaValue = getString(alpha);
					String mikeValue = getString(bravo);

					if ((limaValue == null) && (mikeValue == null))
					{
						result = 0;
					}
					else if (limaValue == null)
					{
						result = -1;
					}
					else if (mikeValue == null)
					{
						result = 1;
					}
					else
					{
						result = limaValue.compareTo(mikeValue);
					}
				}
				break;

				case LONG:
				{
					Long limaValue = getLong(alpha);
					Long mikeValue = getLong(bravo);

					if ((limaValue == null) && (mikeValue == null))
					{
						result = 0;
					}
					else if (limaValue == null)
					{
						result = -1;
					}
					else if (mikeValue == null)
					{
						result = 1;
					}
					else
					{
						result = limaValue.compareTo(mikeValue);
					}
				}
				break;

				default:
					result = 0;
			}
		}

		//
		return result;
	}

	/**
	 * Gets the long.
	 * 
	 * @param source
	 *            the source
	 * @return the long
	 */
	public static Long getLong(final StringLongPair source)
	{
		Long result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.getLong();
		}

		//
		return result;
	}

	/**
	 * Gets the string.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String getString(final StringLongPair source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.getString();
		}

		//
		return result;
	}
}
