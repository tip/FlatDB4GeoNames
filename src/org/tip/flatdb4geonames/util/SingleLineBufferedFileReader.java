/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;

/**
 * The Class SingleLineBufferedFileReader.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class SingleLineBufferedFileReader
{
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private File file;
	private BufferedReader in;
	private String line;

	/**
	 * Instantiates a new single line buffered file reader.
	 * 
	 * @param file
	 *            the file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public SingleLineBufferedFileReader(final File file) throws IOException
	{
		this.file = file;
		this.in = new BufferedReader(new InputStreamReader(new FileInputStream(file), DEFAULT_CHARSET_NAME));
		this.line = this.in.readLine();
	}

	/**
	 * Close.
	 */
	public void close()
	{
		IOUtils.closeQuietly(this.in);
	}

	/**
	 * Gets the file.
	 * 
	 * @return the file
	 */
	public File getFile()
	{
		return this.file;
	}

	/**
	 * Gets the line.
	 * 
	 * @return the line
	 */
	public String getLine()
	{
		String result;

		result = this.line;

		//
		return result;
	}

	/**
	 * Read line.
	 * 
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String readLine() throws IOException
	{
		String result;

		result = this.line;
		this.line = this.in.readLine();

		//
		return result;
	}
}
