/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class SingleLineBufferedFileReaders.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 * 
 */
public class SingleLineBufferedFileReaders extends ArrayList<SingleLineBufferedFileReader>
{
	private static final long serialVersionUID = 7157880846569662370L;

	/**
	 * Instantiates a new single line buffered file readers.
	 */
	public SingleLineBufferedFileReaders()
	{
		super();
	}

	/**
	 * Instantiates a new single line buffered file readers.
	 * 
	 * @param capacity
	 *            the capacity
	 */
	public SingleLineBufferedFileReaders(final int capacity)
	{
		super(capacity);
	}

	/**
	 * Sort.
	 */
	public void sort()
	{
		sort(new SingleLineBufferedFileReaderComparator());
	}

	/**
	 * Sort.
	 * 
	 * @param comparator
	 *            the comparator
	 */
	public void sort(final SingleLineBufferedFileReaderComparator comparator)
	{
		Collections.sort(this, comparator);
	}
}
