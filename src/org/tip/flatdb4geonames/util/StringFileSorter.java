/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class StringFileSorter.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class StringFileSorter
{
	private static Logger logger = LoggerFactory.getLogger(StringFileSorter.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int DEFAULT_SPLIT_LIMIT = 2000000;

	/**
	 * Sort big string file.
	 * 
	 * @param source
	 *            the source
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void sortBigStringFile(final File source) throws IOException
	{
		sortBigStringFile(source, DEFAULT_SPLIT_LIMIT);
	}

	/**
	 * Sort big string file.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param lineLimit
	 *            the line limit
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void sortBigStringFile(final File source, final File target, final int lineLimit) throws IOException
	{
		sortBigStringFile(source, target, lineLimit, null);
	}

	/**
	 * Sort big string file.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param lineLimit
	 *            the line limit
	 * @param comparator
	 *            the comparator
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void sortBigStringFile(final File source, final File target, final int lineLimit, final Comparator<String> comparator) throws IOException
	{
		logger.debug("sort big string file starting...");

		//
		logger.debug("split file start...");
		Chronometer chrono = new Chronometer();
		Files splitFiles = AutomaticFileSplitter.splitFile(source, lineLimit);
		logger.debug("split file done. {}", chrono.stop().interval());

		System.gc();
		logger.debug(" memory={} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);

		//
		logger.debug("sort files start...");
		chrono.reset();
		for (File file : splitFiles)
		{
			logger.debug("\tsort " + file.toString());
			Chronometer chronoStep = new Chronometer();
			sortStringFile(file, comparator);
			logger.debug("\tsort done. {}", chronoStep.stop().interval());
		}
		logger.debug("sort done. {}", chrono.stop().interval());

		System.gc();
		logger.debug(" memory={} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);

		//
		logger.debug("concatenate sorting start...");
		chrono.reset();
		FileSortingConcatenator.concatenateSorting(splitFiles, target, comparator);
		logger.debug("concatenate sorting done. {}", chrono.stop().interval());

		//
		logger.debug("delete temporary files start...");
		for (File file : splitFiles)
		{
			boolean deleteStatus = file.delete();
			if (!deleteStatus)
			{
				logger.error("Failed to delete file [{}]", target.getAbsolutePath());
			}

		}
		logger.debug("delete temporary files done.");

		logger.debug("sort big string file done.");
	}

	/**
	 * Sort big string file.
	 * 
	 * @param source
	 *            the source
	 * @param lineLimit
	 *            the line limit
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void sortBigStringFile(final File source, final int lineLimit) throws IOException
	{
		File target = new File(source.getAbsolutePath() + ".sorted" + System.currentTimeMillis());

		sortBigStringFile(source, target, lineLimit);

		target.renameTo(source);
	}

	/**
	 * Sort big string file.
	 * 
	 * @param source
	 *            the source
	 * @param lineLimit
	 *            the line limit
	 * @param comparator
	 *            the comparator
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void sortBigStringFile(final File source, final int lineLimit, final Comparator<String> comparator) throws IOException
	{
		File target = new File(source.getAbsolutePath() + ".sorted" + System.currentTimeMillis());

		sortBigStringFile(source, target, lineLimit, comparator);

		target.renameTo(source);
	}

	/**
	 * Sort string file.
	 * 
	 * @param source
	 *            the source
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void sortStringFile(final File source) throws IOException
	{
		sortStringFile(source, null);
	}

	/**
	 * Sort string file.
	 * 
	 * @param source
	 *            the source
	 * @param comparator
	 *            the comparator
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void sortStringFile(final File source, final Comparator<String> comparator) throws IOException
	{
		List<String> lines = FileUtils.readLines(source);

		Collections.sort(lines, comparator);

		File target = new File(source.getAbsolutePath() + ".sorted" + System.currentTimeMillis());
		FileUtils.writeLines(target, lines);

		boolean deleteStatus = source.delete();
		if (!deleteStatus)
		{
			logger.error("Failed to delete file [{}]", source.getAbsolutePath());
		}

		target.renameTo(source);
	}
}
