/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * The Class BufferedStringListReader.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 * 
 */
public class BufferedStringListReader implements StringListReader
{
	private BufferedReader in;

	/**
	 * Instantiates a new buffered string list reader.
	 * 
	 * @param in
	 *            the in
	 */
	public BufferedStringListReader(final BufferedReader in)
	{
		this.in = in;
	}

	/* (non-Javadoc)
	 * @see org.tip.flatdb4geonames.util.StringListReader#close()
	 */
	@Override
	public void close()
	{

	}

	/* (non-Javadoc)
	 * @see org.tip.flatdb4geonames.util.StringListReader#readLine()
	 */
	@Override
	public String readLine() throws IOException
	{
		String result;

		result = this.in.readLine();

		//
		return result;
	}
}
