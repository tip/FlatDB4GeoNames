/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.File;

import org.apache.tools.ant.taskdefs.Expand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Unzipper.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Unzipper
{
	private static Logger logger = LoggerFactory.getLogger(Unzipper.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Unzip.
	 * 
	 * @param zipFile
	 *            the zip file
	 * @param targetDirectory
	 *            the target directory
	 */
	public static void unzip(final File zipFile, final File targetDirectory)
	{
		Expand unzipper = new Expand();
		unzipper.setSrc(zipFile);
		unzipper.setDest(targetDirectory);
		unzipper.execute();
	}
}
