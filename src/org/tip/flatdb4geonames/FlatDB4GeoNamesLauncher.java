/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.cli.FlatDB4GeoNamesCLI;
import org.tip.flatdb4geonames.gui.FlatDB4GeoNamesGUI;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;

/**
 * The Class FlatDB4GeoNamesLauncher.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class FlatDB4GeoNamesLauncher
{
	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNamesLauncher.class);

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public static void main(final String[] args) throws IOException, FlatDB4GeoNamesException
	{
		// Configure log.
		File loggerConfig = new File("log4j.properties");
		if (loggerConfig.exists())
		{
			PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
			logger.info("Dedicated log configuration done.");
			logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
		}
		else
		{
			BasicConfigurator.configure();
			logger.info("Basic log configuration done.");
			logger.info("Configuration file was not found in [{}].", loggerConfig.getAbsoluteFile());
		}

		// Manage parameters.
		if (args.length > 0)
		{
			FlatDB4GeoNamesCLI.run(args);
		}
		else if (args.length == 0)
		{
			FlatDB4GeoNamesGUI.instance().run();
		}
	}
}
