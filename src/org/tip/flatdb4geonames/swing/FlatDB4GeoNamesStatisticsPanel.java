/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.swing;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.tip.flatdb4geonames.gui.views.help.TutorialDialog;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.GeoNamesFlatDatabase;

import fr.devinsy.util.FileTools;
import fr.devinsy.util.StringList;

/**
 * The Class FlatDB4GeoNamesStatisticsPanel.
 */
public class FlatDB4GeoNamesStatisticsPanel extends JPanel
{
	private static final long serialVersionUID = -3689363226086057052L;
	private JEditorPane editorPane;

	/**
	 * Instantiates a new flat DB 4 geo names statistics panel.
	 * 
	 * @param report
	 *            the report
	 */
	public FlatDB4GeoNamesStatisticsPanel(final StringList report)
	{
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		this.editorPane = new JEditorPane();
		this.editorPane.setEditable(false);
		this.editorPane.setContentType("text/html");
		scrollPane.setViewportView(this.editorPane);

		//
		try
		{
			refreshReport(report);
		}
		catch (IOException exception)
		{
			exception.printStackTrace();
			this.editorPane.setText("ERROR: " + exception.getMessage());
		}
	}

	/**
	 * Refresh report.
	 * 
	 * @param report
	 *            the report
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void refreshReport(final StringList report) throws IOException
	{
		GeoNamesFlatDatabase database = FlatDB4GeoNames.instance();

		if (database == null)
		{
			this.editorPane.setText("");
		}
		else
		{
			StringList page = new StringList(FileTools.load(TutorialDialog.class.getResource("/org/tip/flatdb4geonames/swing/default.html")).split("\n"));
			page.add(page.size() - 2, report.toString());
			this.editorPane.setText(page.toString());
			this.editorPane.setCaretPosition(0);
		}
	}
}
