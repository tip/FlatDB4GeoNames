/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.gui.views.help.TutorialDialog;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;
import org.tip.flatdb4geonames.model.GeoNamesFlatDatabase;
import org.tip.flatdb4geonames.model.GeoNamesLine;
import org.tip.flatdb4geonames.model.GeoNamesLines;
import org.tip.flatdb4geonames.model.GeoNamesSearchCriteria;
import org.tip.flatdb4geonames.model.GeoNamesSearchResult;
import org.tip.flatdb4geonames.model.index.FeatureClass;
import org.tip.flatdb4geonames.util.Chronometer;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.FileTools;
import fr.devinsy.util.StringList;

/**
 * The Class FlatDB4GeoNamesRequestPanel.
 */
public class FlatDB4GeoNamesRequestPanel extends JPanel
{
	private static final long serialVersionUID = -6719667140204170550L;

	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNamesRequestPanel.class);

	private JTextField txtfldInput;
	private JEditorPane resultPane;
	private JCheckBox chckbxCountryStateRegion;
	private JCheckBox chckbxStreamlake;
	private JCheckBox chckbxParkArea;
	private JCheckBox chckbxCityVillage;
	private JCheckBox chckbxSpotBuildingFarm;
	private JCheckBox chckbxMountainHillRock;
	private JCheckBox chckbxUndersea;
	private JCheckBox chckbxForestHeath;
	private JButton btnSearch;
	private JLabel lblTips;

	/**
	 * Instantiates a new FlatDB4GeoNames request panel.
	 */
	public FlatDB4GeoNamesRequestPanel()
	{
		setLayout(new BorderLayout(0, 0));

		JPanel queryPanel = new JPanel();
		add(queryPanel, BorderLayout.NORTH);
		queryPanel.setLayout(new BoxLayout(queryPanel, BoxLayout.Y_AXIS));

		JPanel panel1 = new JPanel();
		queryPanel.add(panel1);
		panel1.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblInput = new JLabel("Input:");
		panel1.add(lblInput, "2, 2");

		this.txtfldInput = new JTextField();
		this.txtfldInput.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(final KeyEvent event)
			{
				// Keypressed on input textfield.
				if (event.getKeyCode() == KeyEvent.VK_ENTER)
				{
					FlatDB4GeoNamesRequestPanel.this.btnSearch.doClick();
				}
			}
		});

		panel1.add(this.txtfldInput, "4, 2");
		this.txtfldInput.setColumns(50);

		this.btnSearch = new JButton("Search");
		this.btnSearch.setEnabled(false);
		this.btnSearch.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Search.
				try
				{
					GeoNamesSearchCriteria criteria = getCriteria();

					//
					Chronometer chrono = new Chronometer();
					GeoNamesLines lines = FlatDB4GeoNames.instance().search(criteria);
					chrono.stop();

					logger.debug("result lines count=" + lines.size());

					GeoNamesSearchResult result = new GeoNamesSearchResult(criteria, lines, chrono.interval());

					refreshResult(result);
				}
				catch (IOException exception)
				{
					exception.printStackTrace();
					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

				}
				catch (FlatDB4GeoNamesException exception)
				{
					exception.printStackTrace();
					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel1.add(this.btnSearch, "6, 2");

		this.lblTips = new JLabel("(space = AND, comma = OR)");
		panel1.add(this.lblTips, "4, 3, right, default");

		JPanel panel_5 = new JPanel();
		queryPanel.add(panel_5);
		panel_5.setBorder(new TitledBorder(null, "Feature Class", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.chckbxCountryStateRegion = new JCheckBox("Country State Region");
		panel_5.add(this.chckbxCountryStateRegion, "2, 2");

		this.chckbxParkArea = new JCheckBox("Park Area");
		panel_5.add(this.chckbxParkArea, "4, 2");

		this.chckbxSpotBuildingFarm = new JCheckBox("Spot Building Farm");
		panel_5.add(this.chckbxSpotBuildingFarm, "6, 2");

		this.chckbxUndersea = new JCheckBox("Undersea");
		panel_5.add(this.chckbxUndersea, "8, 2");

		this.chckbxStreamlake = new JCheckBox("Stream Lake");
		panel_5.add(this.chckbxStreamlake, "2, 4");

		this.chckbxCityVillage = new JCheckBox("City Village");
		panel_5.add(this.chckbxCityVillage, "4, 4");

		this.chckbxMountainHillRock = new JCheckBox("Mountain Hill Rock");
		panel_5.add(this.chckbxMountainHillRock, "6, 4");

		this.chckbxForestHeath = new JCheckBox("Forest Heath");
		panel_5.add(this.chckbxForestHeath, "8, 4");

		JSeparator separator = new JSeparator();
		queryPanel.add(separator);

		JPanel resultPanel = new JPanel();
		add(resultPanel, BorderLayout.CENTER);
		resultPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		resultPanel.add(scrollPane);

		this.resultPane = new JEditorPane();
		this.resultPane.setEditable(false);
		this.resultPane.setBackground(new Color(238, 238, 238));
		this.resultPane.setContentType("text/html;charset=UTF-8");
		scrollPane.setViewportView(this.resultPane);

		//
		if (FlatDB4GeoNames.isOpened())
		{
			this.btnSearch.setEnabled(true);
		}
		else
		{
			this.btnSearch.setEnabled(false);
		}
	}

	/**
	 * Gets the criteria.
	 * 
	 * @return the criteria
	 */
	public GeoNamesSearchCriteria getCriteria()
	{
		GeoNamesSearchCriteria result;

		result = new GeoNamesSearchCriteria();

		result.setInput(this.txtfldInput.getText());

		result.featureClasses().clear();
		if (this.chckbxCityVillage.isSelected())
		{
			result.featureClasses().add(FeatureClass.CITY_VILLAGE);
		}

		if (this.chckbxCountryStateRegion.isSelected())
		{
			result.featureClasses().add(FeatureClass.COUNTRY_STATE_REGION);
		}

		if (this.chckbxForestHeath.isSelected())
		{
			result.featureClasses().add(FeatureClass.FOREST_HEATH);
		}

		if (this.chckbxMountainHillRock.isSelected())
		{
			result.featureClasses().add(FeatureClass.MOUNTAIN_HILL_ROCK);
		}

		if (this.chckbxParkArea.isSelected())
		{
			result.featureClasses().add(FeatureClass.PARK_AREA);
		}

		if (this.chckbxSpotBuildingFarm.isSelected())
		{
			result.featureClasses().add(FeatureClass.SPOT_BUILDING_FARM);
		}

		if (this.chckbxStreamlake.isSelected())
		{
			result.featureClasses().add(FeatureClass.STREAM_LAKE);
		}

		if (this.chckbxUndersea.isSelected())
		{
			result.featureClasses().add(FeatureClass.UNDERSEA);
		}

		//
		return result;
	}

	/**
	 * Refresh.
	 */
	public void refresh()
	{
		if (FlatDB4GeoNames.isOpened())
		{
			this.btnSearch.setEnabled(true);
		}
		else
		{
			this.btnSearch.setEnabled(false);
		}
	}

	/**
	 * Refresh result.
	 * 
	 * @param result
	 *            the result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public void refreshResult(final GeoNamesSearchResult result) throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesFlatDatabase database = FlatDB4GeoNames.instance();

		if (database == null)
		{
			this.resultPane.setText("");
		}
		else
		{
			StringList lines = toRaw(result, 100);

			logger.debug("raw lines count={}", lines.size());
			logger.debug("raw lines length={}", lines.length());

			//
			StringList page = new StringList(FileTools.load(TutorialDialog.class.getResource("/org/tip/flatdb4geonames/swing/default.html")).split("\n"));
			page.add(page.size() - 2, lines.toString());
			this.resultPane.setText(page.toString());
			this.resultPane.setCaretPosition(0);
		}
	}

	/**
	 * To raw.
	 * 
	 * @param source
	 *            the source
	 * @param maxLineCount
	 *            the max line count
	 * @return the string list
	 */
	public static StringList toRaw(final GeoNamesSearchResult source, final int maxLineCount)
	{
		StringList result;

		result = new StringList();

		result.append("<p>").append(String.format("%,d results in %.3f second(s)", source.getLines().size(), source.getDuration() / 1000f)).appendln("</p>");

		result.appendln("<p>");
		result.appendln("<table class=\"table_default\">");
		result.appendln("  <tr>");
		result.appendln("    <th>Nr.</th>");
		result.appendln("    <th>GeonameId</th>");
		result.appendln("    <th>Name</th>");
		result.appendln("    <th>Ascii Name</th>");
		result.appendln("    <th>Alternate names</th>");
		result.appendln("    <th>latitude</th>");
		result.appendln("    <th>longitude</th>");
		result.appendln("    <th>Feature Class</th>");
		result.appendln("    <th>Feature Code</th>");
		result.appendln("    <th>Country code</th>");
		result.appendln("    <th>CC2</th>");
		result.appendln("    <th title=\"fipscode\">Admin1 code</th>");
		result.appendln("    <th>Admin2 code</th>");
		result.appendln("    <th>Admin3 code</th>");
		result.appendln("    <th>Admin4 code</th>");
		result.appendln("    <th>Population</th>");
		result.appendln("    <th title=\"in meters\">Elevation</th>");
		result.appendln("    <th>Dem</th>");
		result.appendln("    <th>Timezone</th>");
		result.appendln("    <th>Modification<br/>Date</th>");
		result.appendln("  </tr>");

		/*
		* 		geonameid         : integer id of record in geonames database
		* 		name              : name of geographical point (utf8) varchar(200)
		* 		asciiname         : this.name of geographical point in plain ascii characters, varchar(200)
		* 		alternatenames    : alternatenames, comma separated, ascii names automatically transliterated, convenience attribute from alternatename table, varchar(10000)
		* 		latitude          : latitude in decimal degrees (wgs84)
		* 		longitude         : longitude in decimal degrees (wgs84)
		* 		feature class     : see http://www.geonames.org/export/codes.html, char(1)
		* 		feature code      : see http://www.geonames.org/export/codes.html, varchar(10)
		* 		country code      : ISO-3166 2-letter country code, 2 characters
		* 		cc2               : alternate country codes, comma separated, ISO-3166 2-letter country code, 60 characters
		* 		admin1 code       : fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)
		* 		admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80) 
		* 		admin3 code       : code for third level administrative division, varchar(20)
		* 		admin4 code       : code for fourth level administrative division, varchar(20)
		* 		population        : bigint (8 byte int) 
		* 		elevation         : in meters, integer
		* 		dem               : digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.
		* 		timezone          : the timezone id (see file timeZone.txt) varchar(40)
		* 		modification date : date of last modification in yyyy-MM-dd format
		*/

		for (int lineCount = 0; lineCount < NumberUtils.min(source.getLines().size(), maxLineCount, maxLineCount); lineCount++)
		{
			GeoNamesLine line = source.getLines().get(lineCount);

			result.appendln("  <tr>");
			result.append("    <td class=\"center\">").append(lineCount + 1).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getGeoNameId()).appendln("</td>");
			result.append("    <td class=\"left\">").append(line.getName()).appendln("</td>");
			result.append("    <td class=\"left\">").append(line.getAsciiName()).appendln("</td>");
			result.append("    <td class=\"left\">").append(line.getAlternateNames().toStringList().toStringWithCommas()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getLatitude()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getLongitude()).appendln("</td>");
			result.append("    <td class=\"center\">").append(FeatureClass.toString(line.getFeatureClass())).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getFeatureCode()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getCountryCode()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getAlternateCountryCodes().toStringList().toStringWithCommas()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getAdministrativeCode1()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getAdministrativeCode2()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getAdministrativeCode3()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getAdministrativeCode4()).appendln("</td>");
			result.append("    <td class=\"right\">").append(line.getPopulation()).appendln("</td>");
			result.append("    <td class=\"right\">").append(line.getElevation()).appendln("</td>");
			if (line.getDem() == null)
			{
				result.append("    <td>").append("").appendln("</td>");
			}
			else
			{
				result.append("    <td class=\"center\">").append(line.getDem()).appendln("</td>");
			}
			result.append("    <td class=\"center\">").append(line.getTimeZone()).appendln("</td>");
			result.append("    <td class=\"center\">").append(line.getModificationDate()).appendln("</td>");
			result.appendln("  </tr>");
		}
		result.appendln("</table>");
		result.appendln("</p>");

		//
		return result;
	}

	/**
	 * To tiny.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 */
	public static StringList toTiny(final GeoNamesLines source)
	{
		StringList result;

		result = new StringList();

		//
		return result;
	}
}
