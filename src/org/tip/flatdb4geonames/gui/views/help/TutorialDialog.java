/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.gui.views.help;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.FileTools;

/**
 * The Class TutorialDialog.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class TutorialDialog extends JDialog
{
	private static final long serialVersionUID = 2774313042821986959L;

	private static final ResourceBundle bundle = ResourceBundle.getBundle("org.tip.flatdb4geonames.gui.messages");

	private final JPanel contentPanel = new JPanel();

	/**
	 * Instantiates a new tutorial dialog.
	 */
	public TutorialDialog()
	{
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setTitle("Tutorial");
		setIconImage(Toolkit.getDefaultToolkit().getImage(TutorialDialog.class.getResource("/org/tip/flatdb4geonames/gui/favicon-16x16.jpg")));
		setBounds(100, 100, 700, 560);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(
				new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), }));
		{
			JLabel label = new JLabel("");
			label.setIcon(new ImageIcon(TutorialDialog.class.getResource("/org/tip/flatdb4geonames/gui/views/help/logo-200x.png")));
			this.contentPanel.add(label, "2, 2");
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			this.contentPanel.add(scrollPane, "4, 2, 1, 3, fill, fill");
			{
				JEditorPane txtpnTutorial = new JEditorPane("text/html", "");
				txtpnTutorial.setEditable(false);
				txtpnTutorial.setText("");
				scrollPane.setViewportView(txtpnTutorial);
				// ///////////////////////////:
				try
				{
					txtpnTutorial.setText(FileTools.load(TutorialDialog.class.getResource("/org/tip/flatdb4geonames/gui/views/help/tutorial.html")));
				}
				catch (IOException exception)
				{
					exception.printStackTrace();
				}
				txtpnTutorial.setCaretPosition(0);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		try
		{
			TutorialDialog dialog = new TutorialDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}

	/**
	 * This method show dialog centered with screen.
	 */
	public static void showDialog()
	{
		showDialog(null);
	}

	/**
	 * This method show dialog centered with parent component.
	 * 
	 * @param parent
	 *            parent component of the dialog. If parent is null, dialog is
	 *            centered with screen.
	 */
	public static void showDialog(final Component parent)
	{
		TutorialDialog dialog = new TutorialDialog();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}
}
