/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.gui.views.help;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.tip.flatdb4geonames.util.BuildInformation;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.FileTools;

/**
 * The Class AboutDialog.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AboutDialog extends JDialog
{
	private static final long serialVersionUID = 3415318715821074783L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Instantiates a new about dialog.
	 */
	public AboutDialog()
	{
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setTitle("About FlatDB4GeoNames");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AboutDialog.class.getResource("/org/tip/flatdb4geonames/gui/favicon-16x16.jpg")));
		setBounds(100, 100, 650, 361);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(
				new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), }));
		{
			JLabel label = new JLabel("");
			label.setIcon(new ImageIcon(AboutDialog.class.getResource("/org/tip/flatdb4geonames/gui/views/help/logo-200x.png")));
			this.contentPanel.add(label, "2, 2");
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			this.contentPanel.add(scrollPane, "4, 2, 1, 3, fill, fill");
			{
				JEditorPane txtpnAbout = new JEditorPane("text/html", "");
				txtpnAbout.setEditable(false);
				txtpnAbout.setText("");
				scrollPane.setViewportView(txtpnAbout);
				// ///////////////////////////:
				try
				{
					String aboutText = FileTools.load(AboutDialog.class.getResource("/org/tip/flatdb4geonames/gui/views/help/about.html"));
					aboutText = aboutText.replace("$version", new BuildInformation().version());
					txtpnAbout.setText(aboutText);
				}
				catch (IOException exception)
				{
					exception.printStackTrace();
				}
				txtpnAbout.setCaretPosition(0);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

	/**
	 * This method show dialog centered with screen.
	 */
	public static void showDialog()
	{
		showDialog(null);
	}

	/**
	 * This method show dialog centered with parent component.
	 * 
	 * @param parent
	 *            parent component of the dialog. If parent is null, dialog is
	 *            centered with screen.
	 */
	public static void showDialog(final Component parent)
	{
		AboutDialog dialog = new AboutDialog();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}
}
