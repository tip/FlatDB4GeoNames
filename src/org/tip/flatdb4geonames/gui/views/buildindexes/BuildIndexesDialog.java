/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.gui.views.buildindexes;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesFactory;
import org.tip.flatdb4geonames.util.Chronometer;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * The Class BuildIndexesDialog.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class BuildIndexesDialog extends JDialog
{
	private static final long serialVersionUID = 2320545249835089493L;

	private static Logger logger = LoggerFactory.getLogger(BuildIndexesDialog.class);

	private final JPanel contentPanel = new JPanel();
	private JTextField txtfldSourceDirectory;
	private JTextField txtfldFreeDiskSpace;
	private JButton btnBuild;
	private JButton btnCancel;
	private JLabel lblAllCountriesTxtValue;
	private JLabel lblFeatureCodeEnTxtValue;
	private SwingWorker<Boolean, Integer> buildWorker;
	private SwingWorker<Boolean, Integer> progressWorker;
	private JButton btnDone;
	private JProgressBar progressBarIdSeekIndex;
	private JProgressBar progressBarSeeksIndex;
	private JProgressBar progressBarWordSeekIndex;
	private Chronometer chrono;
	private JLabel lblTimer;
	private JButton btnSelector;
	private JProgressBar progressBarGlobal;

	/**
	 * Instantiates a new builds the indexes dialog.
	 */
	public BuildIndexesDialog()
	{
		setModal(true);
		setTitle("Build Indexes");
		setBounds(100, 100, 630, 445);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new BoxLayout(this.contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel introPanel = new JPanel();
			this.contentPanel.add(introPanel);
			{
				JLabel lblNewLabel = new JLabel(
						"<html>\nThe main GeoNames file takes 1.2GB and contains more than 10 million of entries.<br/>\nTo increase request performance, FlatDB4GeoNames build indexes<br/>\nto reach faster GeoNames data by geonameId and words.<br/>\nThis Panel allows you to build these indexes.\n</html>");
				introPanel.add(lblNewLabel);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(10);
			this.contentPanel.add(verticalStrut);
		}
		{
			JPanel inputPanel = new JPanel();
			this.contentPanel.add(inputPanel);
			inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
					FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
			{
				JLabel lblSource = new JLabel("Source directory:");
				inputPanel.add(lblSource, "2, 2, right, default");
			}
			{
				{
					this.txtfldSourceDirectory = new JTextField();
					this.txtfldSourceDirectory.setEditable(false);
					inputPanel.add(this.txtfldSourceDirectory, "4, 2, fill, default");
					this.txtfldSourceDirectory.setColumns(10);
				}
				this.btnSelector = new JButton("...");
				this.btnSelector.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Select target directory button.
						File target = BuildIndexesSourceSelector.showSelectorDialog(null, null);

						if (target != null)
						{
							setSourceDirectory(target);
						}
					}
				});
				inputPanel.add(this.btnSelector, "6, 2");
			}
			{
				JLabel lblAllCountriesTxt = new JLabel("allCountries.txt:");
				inputPanel.add(lblAllCountriesTxt, "2, 4, right, default");
			}
			{
				this.lblAllCountriesTxtValue = new JLabel("missing");
				inputPanel.add(this.lblAllCountriesTxtValue, "4, 4, center, default");
			}
			{
				JLabel lblFeatureCodesEnTxt = new JLabel("featureCodes_en.txt:");
				inputPanel.add(lblFeatureCodesEnTxt, "2, 6");
			}
			{
				this.lblFeatureCodeEnTxtValue = new JLabel("missing");
				inputPanel.add(this.lblFeatureCodeEnTxtValue, "4, 6, center, default");
			}
			{
				JLabel lblSpaceDisk = new JLabel("Free disk space:");
				inputPanel.add(lblSpaceDisk, "2, 8, right, default");
			}
			{
				this.txtfldFreeDiskSpace = new JTextField();
				this.txtfldFreeDiskSpace.setEditable(false);
				this.txtfldFreeDiskSpace.setHorizontalAlignment(SwingConstants.RIGHT);
				inputPanel.add(this.txtfldFreeDiskSpace, "4, 8, fill, default");
				this.txtfldFreeDiskSpace.setColumns(10);
			}
			{
				this.progressBarGlobal = new JProgressBar();
				inputPanel.add(this.progressBarGlobal, "4, 12");
			}
			{
				JLabel lblNewLabel1 = new JLabel("word_seek.index:");
				inputPanel.add(lblNewLabel1, "2, 14, right, default");
			}
			{
				this.progressBarWordSeekIndex = new JProgressBar();
				inputPanel.add(this.progressBarWordSeekIndex, "4, 14");
			}
			{
				JLabel lblSeeksIndex = new JLabel("seeks.index:");
				inputPanel.add(lblSeeksIndex, "2, 16, right, default");
			}
			{
				this.progressBarSeeksIndex = new JProgressBar();
				inputPanel.add(this.progressBarSeeksIndex, "4, 16");
			}
			{
				JLabel lblIdSeekIndex = new JLabel("id_seek.index:");
				inputPanel.add(lblIdSeekIndex, "2, 18, right, default");
			}
			{
				this.progressBarIdSeekIndex = new JProgressBar();
				inputPanel.add(this.progressBarIdSeekIndex, "4, 18");
			}
			{
				this.lblTimer = new JLabel("00:00:00");
				inputPanel.add(this.lblTimer, "4, 21, center, default");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				this.btnCancel = new JButton("Cancel");
				this.btnCancel.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Cancel button.

						//
						if (BuildIndexesDialog.this.buildWorker != null)
						{
							BuildIndexesDialog.this.buildWorker.cancel(true);
						}

						//
						if (BuildIndexesDialog.this.progressWorker != null)
						{
							BuildIndexesDialog.this.progressWorker.cancel(true);
						}

						//
						if ((BuildIndexesDialog.this.buildWorker != null) && (BuildIndexesDialog.this.progressWorker != null))
						{
							clearTarget(BuildIndexesDialog.this.txtfldSourceDirectory.getText());
						}

						//
						dispose();
					}
				});
				this.btnCancel.setActionCommand("Cancel");
				buttonPane.add(this.btnCancel);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				buttonPane.add(horizontalStrut);
			}
			{
				this.btnBuild = new JButton("Build");
				this.btnBuild.setEnabled(false);
				this.btnBuild.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Build button.
						BuildIndexesDialog.this.btnBuild.setEnabled(false);
						BuildIndexesDialog.this.btnSelector.setEnabled(false);
						final String targetDirectory = BuildIndexesDialog.this.txtfldSourceDirectory.getText();

						BuildIndexesDialog.this.chrono = new Chronometer();

						//
						BuildIndexesDialog.this.buildWorker = new SwingWorker<Boolean, Integer>()
						{
							/* (non-Javadoc)
							 * @see javax.swing.SwingWorker#doInBackground()
							 */
							@Override
							protected Boolean doInBackground()
							{
								Boolean result;

								try
								{
									clearTarget(targetDirectory);

									FlatDB4GeoNamesFactory.buildIndex(new File(targetDirectory));

									result = true;
								}
								catch (FileNotFoundException exception)
								{
									exception.printStackTrace();
									logger.error("Technical problem detected (1).", exception);

									String title = "Error";
									String message = "File not found: " + exception.getMessage();

									JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

									result = false;
								}
								catch (Exception exception)
								{
									exception.printStackTrace();
									logger.error("Technical problem detected (2).", exception);

									String title = "Error";
									String message = "Download error: " + exception.getMessage();

									JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

									result = false;
								}

								//
								return result;
							}

							/*
							 * Can safely update the GUI from this method. 
							 * 
							 *  (non-Javadoc)
							 * @see javax.swing.SwingWorker#done()
							 */
							@Override
							protected void done()
							{
								// Retrieve the return value of
								// doInBackground.
								logger.debug("build index progress done");

								BuildIndexesDialog.this.btnCancel.setEnabled(false);
								BuildIndexesDialog.this.btnBuild.setEnabled(false);
								BuildIndexesDialog.this.btnDone.setEnabled(true);
								BuildIndexesDialog.this.progressWorker.cancel(true);
								BuildIndexesDialog.this.progressBarGlobal.setValue(100);
								BuildIndexesDialog.this.progressBarIdSeekIndex.setValue(100);
								BuildIndexesDialog.this.progressBarSeeksIndex.setValue(100);
								BuildIndexesDialog.this.progressBarWordSeekIndex.setValue(100);
							}
						};
						BuildIndexesDialog.this.buildWorker.execute();

						//
						BuildIndexesDialog.this.progressWorker = new SwingWorker<Boolean, Integer>()
						{
							@Override
							protected Boolean doInBackground() throws InterruptedException
							{
								boolean ended = false;
								while (!ended)
								{
									Thread.sleep(1000);
									publish();
								}

								return true;
							}

							/* Can safely update the GUI from this method.
							 * 
							 *  (non-Javadoc)
							 * @see javax.swing.SwingWorker#done()
							 */
							@Override
							protected void done()
							{
								logger.debug("progressworker done");
							}

							/*
							 * Can safely update the GUI from this method.
							 * 
							 *  (non-Javadoc)
							 * @see javax.swing.SwingWorker#process(java.util.List)
							 */
							@Override
							protected void process(final List<Integer> chunks)
							{
								// logger.debug("progress worker process");

								//
								updateFreeDiskSpace();

								//
								if (new File(targetDirectory, "id_seek.index").exists())
								{
									BuildIndexesDialog.this.progressBarIdSeekIndex.setValue(100);
								}

								//
								if (new File(targetDirectory, "seeks.index").exists())
								{
									BuildIndexesDialog.this.progressBarSeeksIndex.setValue(100);
								}

								//
								if (new File(targetDirectory, "word_seek.index").exists())
								{
									BuildIndexesDialog.this.progressBarWordSeekIndex.setValue(100);
								}

								//
								long duration = BuildIndexesDialog.this.chrono.stop().interval();
								BuildIndexesDialog.this.progressBarGlobal.setValue((int) (duration * 100 / (25 * 60 * 1000)));

								//
								String timerValue = Chronometer.toTimer(duration);
								BuildIndexesDialog.this.lblTimer.setText(timerValue);
							}
						};

						BuildIndexesDialog.this.progressWorker.execute();
					}
				});
				this.btnBuild.setActionCommand("OK");
				buttonPane.add(this.btnBuild);
				getRootPane().setDefaultButton(this.btnBuild);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				buttonPane.add(horizontalStrut);
			}
			{
				this.btnDone = new JButton("Done");
				this.btnDone.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Button done.
						dispose();
					}
				});
				this.btnDone.setEnabled(false);
				this.btnDone.setActionCommand("OK");
				buttonPane.add(this.btnDone);
			}
		}
	}

	/**
	 * Clear target.
	 * 
	 * @param path
	 *            the path
	 */
	public void clearTarget(final String path)
	{
		if (StringUtils.isNotBlank(path))
		{
			new File(path, "id_seek.index").delete();
			new File(path, "seeks.index").delete();
			new File(path, "word_seek.index").delete();
			new File(path, "word_seeks.index.rejection").delete();
		}
	}

	/**
	 * Sets the source directory.
	 * 
	 * @param target
	 *            the new source directory
	 */
	public void setSourceDirectory(final File target)
	{
		if ((target != null) && (target.exists()) && (target.isDirectory()))
		{
			if (target.canWrite())
			{
				BuildIndexesDialog.this.txtfldSourceDirectory.setText(target.getAbsolutePath());

				String value = String.format("%,dMB", target.getUsableSpace() / 1024 / 1024);
				this.txtfldFreeDiskSpace.setText(value);

				File allCountriesFile = new File(target + File.separator + "allCountries.txt");
				File featureCodesFile = new File(target + File.separator + "featureCodes_en.txt");

				if (allCountriesFile.exists())
				{
					this.lblAllCountriesTxtValue.setText("found");
				}
				else
				{
					this.lblAllCountriesTxtValue.setText("missing");
				}

				if (featureCodesFile.exists())
				{
					this.lblFeatureCodeEnTxtValue.setText("found");
				}
				else
				{
					this.lblFeatureCodeEnTxtValue.setText("missing");
				}

				if ((allCountriesFile.exists()) && (featureCodesFile.exists()))
				{
					this.btnBuild.setEnabled(true);
				}
				else
				{
					String title = "Warning";
					String message = "One GeoNames file is missing. Please restart download.";

					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
			else
			{
				String title = "Warning";
				String message = "This directory is not writable.";

				JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Update free disk space.
	 */
	public void updateFreeDiskSpace()
	{
		String value = String.format("%,dMB", new File(this.txtfldSourceDirectory.getText()).getUsableSpace() / 1024 / 1024);
		this.txtfldFreeDiskSpace.setText(value);
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		try
		{
			new BuildIndexesDialog();
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}

	/**
	 * Launch the application.
	 */
	public static void showDialog()
	{
		showDialog(null);
	}

	/**
	 * Launch the application.
	 * 
	 * @param parent
	 *            the parent
	 */
	public static void showDialog(final Component parent)
	{
		//
		BuildIndexesDialog dialog = new BuildIndexesDialog();
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}
}
