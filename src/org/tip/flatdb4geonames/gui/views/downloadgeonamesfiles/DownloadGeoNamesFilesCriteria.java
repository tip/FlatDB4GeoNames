/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.gui.views.downloadgeonamesfiles;

/**
 * The Class DownloadGeoNamesFilesCriteria.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class DownloadGeoNamesFilesCriteria
{
	private String sourceRepository;
	private String targetDirectory;

	/**
	 * Instantiates a new download geo names files criteria.
	 */
	public DownloadGeoNamesFilesCriteria()
	{
		this.sourceRepository = null;
		this.targetDirectory = null;
	}

	/**
	 * Gets the source repository.
	 * 
	 * @return the source repository
	 */
	public String getSourceRepository()
	{
		return this.sourceRepository;
	}

	/**
	 * Gets the target directory.
	 * 
	 * @return the target directory
	 */
	public String getTargetDirectory()
	{
		return this.targetDirectory;
	}

	/**
	 * Sets the source repository.
	 * 
	 * @param sourceRepository
	 *            the new source repository
	 */
	public void setSourceRepository(final String sourceRepository)
	{
		this.sourceRepository = sourceRepository;
	}

	/**
	 * Sets the target directory.
	 * 
	 * @param targetDirectory
	 *            the new target directory
	 */
	public void setTargetDirectory(final String targetDirectory)
	{
		this.targetDirectory = targetDirectory;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = String.format("[%s, %s]", this.sourceRepository, this.targetDirectory);

		//
		return result;
	}
}
