/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.gui.util;

import java.util.ArrayList;
import java.util.List;

import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GUIToolBox.
 * 
 * @author TIP
 */
public class GUIToolBox
{
	private static Logger logger = LoggerFactory.getLogger(GUIToolBox.class);

	/**
	 * Available look and feels.
	 * 
	 * @return the list
	 */
	public static List<String> availableLookAndFeels()
	{
		List<String> result;

		result = new ArrayList<String>();

		for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
		{
			result.add(info.getName());
		}

		//
		return result;
	}
}
