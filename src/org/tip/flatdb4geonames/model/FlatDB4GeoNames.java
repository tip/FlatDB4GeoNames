/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FlatDB4GeoNames.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class FlatDB4GeoNames
{
	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNames.class);

	private static GeoNamesFlatDatabase instance = null;

	/**
	 * Builds the index.
	 * 
	 * @param localRepository
	 *            the local repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void buildIndex(final File localRepository) throws IOException
	{
		FlatDB4GeoNamesFactory.buildIndex(localRepository);
	}

	/**
	 * Close.
	 */
	public static void close()
	{
		if (instance == null)
		{
			throw new IllegalStateException("None instance.");
		}
		else
		{
			instance.close();
			instance = null;
		}
	}

	/**
	 * Downloads required files from the official GeoNames repository to a local
	 * repository.
	 * 
	 * @param sourceRepositoryUrl
	 *            the source repository url
	 * @param localTargetRepository
	 *            the local target repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void downloadDatabase(final String sourceRepositoryUrl, final File localTargetRepository) throws IOException
	{
		FlatDB4GeoNamesFactory.downloadDatabase(sourceRepositoryUrl, localTargetRepository);
	}

	/**
	 * Downloads required files from the official GeoNames repository to a local
	 * repository.
	 * 
	 * @param localTargetRepository
	 *            the local target repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void downloadGeoNamesFiles(final File localTargetRepository) throws IOException
	{
		FlatDB4GeoNamesFactory.downloadGeoNamesFiles(localTargetRepository);
	}

	/**
	 * Download geo names files.
	 * 
	 * @param sourceRepositoryUrl
	 *            the source repository url
	 * @param localTargetRepository
	 *            the local target repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void downloadGeoNamesFiles(final String sourceRepositoryUrl, final File localTargetRepository) throws IOException
	{
		FlatDB4GeoNamesFactory.downloadGeoNamesFiles(sourceRepositoryUrl, localTargetRepository);
	}

	/**
	 * Instance.
	 * 
	 * @return the geo names flat database
	 */
	public static GeoNamesFlatDatabase instance()
	{
		GeoNamesFlatDatabase result;

		result = instance;

		//
		return result;
	}

	/**
	 * Checks if is opened.
	 * 
	 * @return true, if is opened
	 */
	public static boolean isOpened()
	{
		boolean result;

		if (instance == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Checks if is valid database.
	 * 
	 * @param directory
	 *            the directory
	 * @return true, if is valid database
	 */
	public static boolean isValidDatabase(final File directory)
	{
		boolean result;

		result = FlatDB4GeoNamesFactory.isValidDatabase(directory);

		//
		return result;
	}

	/**
	 * Open.
	 * 
	 * @param repository
	 *            the repository
	 * @return the geo names flat database
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static GeoNamesFlatDatabase open(final File repository) throws IOException
	{
		GeoNamesFlatDatabase result;

		if (instance != null)
		{
			close();
		}

		instance = FlatDB4GeoNamesFactory.open(repository);
		result = instance;

		//
		return result;
	}

	/**
	 * Open.
	 * 
	 * @param repository
	 *            the repository
	 * @return the geo names flat database
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static GeoNamesFlatDatabase open(final String repository) throws IOException
	{
		GeoNamesFlatDatabase result;

		instance = FlatDB4GeoNamesFactory.open(repository);
		result = instance;

		//
		return result;
	}
}
