/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.GeoNamesSearchCriteria.Continent;
import org.tip.flatdb4geonames.model.GeoNamesSearchCriteria.SearchMode;
import org.tip.flatdb4geonames.model.index.AdministrativePath;
import org.tip.flatdb4geonames.model.index.FeatureClass;
import org.tip.flatdb4geonames.model.index.IndexOfAdministrativePaths;
import org.tip.flatdb4geonames.model.index.IndexOfFeatures;
import org.tip.flatdb4geonames.model.index.IndexOfIdSeek;
import org.tip.flatdb4geonames.model.index.IndexOfWordSeeks;
import org.tip.flatdb4geonames.model.index.IndexOfWordSeeksBuilder;
import org.tip.flatdb4geonames.util.StringLengthComparator;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;

/**
 * The Class GeoNamesFlatDatabase.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeoNamesFlatDatabase
{
	private static Logger logger = LoggerFactory.getLogger(GeoNamesFlatDatabase.class);

	public static final String GEONAMES_DUMP_REPOSITORY = "http://download.geonames.org/export/dump/";

	public static final String GEONAMES_MAIN_FILENAME = "allCountries.txt";

	public static final String GEONAMES_FEATURES_FILE = "featureCodes_en.txt";

	public static final String GEONAMES_ADMINISTRATIVE_CODE1_FILE = "admin1CodesASCII.txt";
	public static final String GEONAMES_ADMINISTRATIVE_CODE2_FILE = "admin2Codes.txt";

	private File repository;
	private IndexOfIdSeek indexOfIdSeek;
	private IndexOfWordSeeks indexOfWordSeeks;
	private IndexOfFeatures indexOfFeatures;
	private IndexOfAdministrativePaths indexOfAdministrativePaths;

	/**
	 * Instantiates a new geo names flat database.
	 * 
	 * @param repository
	 *            the repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public GeoNamesFlatDatabase(final File repository) throws IOException
	{
		if (repository == null)
		{
			throw new IllegalArgumentException("Bad repository (null).");
		}
		else if (!repository.exists())
		{
			throw new IllegalArgumentException("Bad repository (not existing) " + repository.getAbsolutePath());
		}
		else if (!repository.isDirectory())
		{
			throw new IllegalArgumentException("Bad repository (not directory) " + repository.getAbsolutePath());
		}
		else if (!repository.canRead())
		{
			throw new IllegalArgumentException("Bad repository (not readable) " + repository.getAbsolutePath());
		}
		else
		{
			this.repository = repository;

			open();
		}
	}

	/**
	 * Close.
	 */
	public void close()
	{
		this.indexOfIdSeek.close();
		this.indexOfIdSeek = null;

		this.indexOfWordSeeks.close();
		this.indexOfWordSeeks = null;

		this.indexOfFeatures.clear();
		this.indexOfFeatures = null;

		this.indexOfAdministrativePaths.clear();
		this.indexOfAdministrativePaths = null;
	}

	/**
	 * Gets the repository.
	 * 
	 * @return the repository
	 */
	public File getRepository()
	{
		return this.repository;
	}

	/**
	 * Open.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void open() throws IOException
	{
		if ((this.repository == null) || (!this.repository.isDirectory()))
		{
			throw new IllegalArgumentException("Bad repository.");
		}
		else
		{
			File geonamesFile = new File(this.repository.getAbsoluteFile() + File.separator + GEONAMES_MAIN_FILENAME);

			logger.debug("memory={} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);
			this.indexOfFeatures = new IndexOfFeatures(new File(this.repository.getAbsoluteFile() + File.separator + GEONAMES_FEATURES_FILE));

			logger.debug("memory={} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);
			this.indexOfIdSeek = new IndexOfIdSeek(geonamesFile, this.repository);

			this.indexOfWordSeeks = new IndexOfWordSeeks(geonamesFile, this.repository);

			logger.debug("memory={} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);
			this.indexOfAdministrativePaths = new IndexOfAdministrativePaths(new File(this.repository.getAbsoluteFile() + File.separator + GEONAMES_ADMINISTRATIVE_CODE1_FILE), new File(
					this.repository.getAbsoluteFile() + File.separator + GEONAMES_ADMINISTRATIVE_CODE2_FILE));
		}
	}

	/**
	 * Search by generic input. Generic input is concatenation of single input
	 * separated by comma. Single input contains word separated by separator
	 * characters (space...).
	 * 
	 * <ul>
	 * <li>comma = OR</li>
	 * <li>space = AND</li>
	 * </ul>
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the geo names lines
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public GeoNamesLines search(final GeoNamesSearchCriteria criteria) throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesLines result;

		if (criteria == null)
		{
			result = new GeoNamesLines();
		}
		else
		{
			if (NumberUtils.isDigits(criteria.getInput()))
			{
				result = new GeoNamesLines();

				GeoNamesLine line = searchByGeoNameId(Integer.parseInt(criteria.getInput()));

				if (line != null)
				{
					result.add(line);
				}
			}
			else
			{
				String toponyms[] = criteria.getInput().split(",");

				if (toponyms.length == 1)
				{
					result = searchBySingleInput(criteria);
				}
				else
				{
					result = new GeoNamesLines();

					HashSet<Long> index = new HashSet<Long>();
					GeoNamesSearchCriteria subCriteria = new GeoNamesSearchCriteria(criteria);
					for (String toponym : toponyms)
					{
						logger.debug("sub=[{}]", toponym);
						subCriteria.setInput(toponym);
						GeoNamesLines subResult = searchBySingleInput(subCriteria);

						for (GeoNamesLine line : subResult)
						{
							if (!index.contains(line.getGeoNameId()))
							{
								result.add(line);
								index.add(line.getGeoNameId());
							}
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * Search by generic input.
	 * 
	 * @param input
	 *            the input
	 * @return the geo names lines
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public GeoNamesLines search(final String input) throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesLines result;

		if (input == null)
		{
			result = new GeoNamesLines();
		}
		else
		{
			GeoNamesSearchCriteria criteria = new GeoNamesSearchCriteria();

			criteria.setSearchMode(SearchMode.PART_MATCH);
			criteria.setContinentScope(Continent.ALL);
			criteria.setInput(input);

			result = search(criteria);
		}

		//
		return result;
	}

	/**
	 * Search by generic input.
	 * 
	 * @param input
	 *            the input
	 * @param featureClasses
	 *            the feature classes
	 * @return the geo names lines
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public GeoNamesLines search(final String input, final FeatureClass... featureClasses) throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesLines result;

		if (input == null)
		{
			result = new GeoNamesLines();
		}
		else
		{
			GeoNamesSearchCriteria criteria = new GeoNamesSearchCriteria();

			criteria.setSearchMode(SearchMode.PART_MATCH);
			criteria.featureClasses().addAll(featureClasses);
			criteria.setContinentScope(Continent.ALL);
			criteria.setInput(input);

			result = search(criteria);
		}

		//
		return result;
	}

	/**
	 * Search for the geoNameId of an administrative code path.
	 * 
	 * An administrative code path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * Examples of administrative path:
	 * 
	 * <pre>
	 * searchAdministrativeGeoNameId("US")        = "United-States"
	 * searchAdministrativeGeoNameId("US.KY")     = "Kentucky"
	 * searchAdministrativeGeoNameId("US.KY.017") = "Bourbon County"
	 * </pre>
	 * 
	 * @param administrativeCodePath
	 *            the administrative code path
	 * @return the integer
	 */
	public Integer searchAdministrativeGeoNameId(final String administrativeCodePath)
	{
		Integer result;

		AdministrativePath path = this.indexOfAdministrativePaths.get(administrativeCodePath);

		if (path == null)
		{
			result = null;
		}
		else
		{
			result = path.getGeonameId();
		}

		//
		return result;
	}

	/**
	 * Search for the geoNameId of an administrative code path.
	 * 
	 * An administrative code path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * Examples of administrative path:
	 * 
	 * <pre>
	 * searchAdministrativeGeoNameId("US", null, null, null) = "United-States"
	 * searchAdministrativeGeoNameId("US", "KY", null, null) = "Kentucky"
	 * searchAdministrativeGeoNameId("US", "KY", "017", null) = "Bourbon County"
	 * </pre>
	 * 
	 * @param countryCode
	 *            the country code
	 * @param adminCode1
	 *            the admin code 1
	 * @param adminCode2
	 *            the admin code 2
	 * @param adminCode3
	 *            the admin code 3
	 * @param adminCode4
	 *            the admin code 4
	 * @return the integer
	 */
	public Integer searchAdministrativeGeoNameId(final String countryCode, final String adminCode1, final String adminCode2, final String adminCode3, final String adminCode4)
	{
		Integer result;

		AdministrativePath path = this.indexOfAdministrativePaths.get(countryCode, adminCode1, adminCode2, adminCode3, adminCode4);

		if (path == null)
		{
			result = null;
		}
		else
		{
			result = path.getGeonameId();
		}

		//
		return result;
	}

	/**
	 * Search for the toponym of an administrative code path.
	 * 
	 * An administrative code path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * Examples of administrative path:
	 * 
	 * <pre>
	 * searchAdministrativeToponym("US")        = "United-States"
	 * searchAdministrativeToponym("US.KY")     = "Kentucky"
	 * searchAdministrativeToponym("US.KY.017") = "Bourbon County"
	 * </pre>
	 * 
	 * @param administrativeCodePath
	 *            the administrative code path
	 * @return the string
	 */
	public String searchAdministrativeToponym(final String administrativeCodePath)
	{
		String result;

		AdministrativePath path = this.indexOfAdministrativePaths.get(administrativeCodePath);

		if (path == null)
		{
			result = null;
		}
		else
		{
			result = path.getToponym();
		}

		//
		return result;
	}

	/**
	 * Search for the toponym of an administrative code path.
	 * 
	 * An administrative code path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * Examples of administrative path:
	 * 
	 * <pre>
	 * searchAdministrativeToponym("US", null, null, null)  = "United-States"
	 * searchAdministrativeToponym("US", "KY", null, null)  = "Kentucky"
	 * searchAdministrativeToponym("US", "KY", "017", null) = "Bourbon County"
	 * </pre>
	 * 
	 * @param countryCode
	 *            the country code
	 * @param adminCode1
	 *            the admin code 1
	 * @param adminCode2
	 *            the admin code 2
	 * @param adminCode3
	 *            the admin code 3
	 * @param adminCode4
	 *            the admin code 4
	 * @return the string
	 */
	public String searchAdministrativeToponym(final String countryCode, final String adminCode1, final String adminCode2, final String adminCode3, final String adminCode4)
	{
		String result;

		AdministrativePath path = this.indexOfAdministrativePaths.get(countryCode, adminCode1, adminCode2, adminCode3, adminCode4);

		if (path == null)
		{
			result = null;
		}
		else
		{
			result = path.getToponym();
		}

		//
		return result;
	}

	/**
	 * Search for the toponym path of an administrative code path.
	 * 
	 * An administrative code path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * Examples of administrative path:
	 * 
	 * <pre>
	 * searchAdministrativeToponymPath("US")        = "United-States"
	 * searchAdministrativeToponymPath("US.KY")     = "United-States / Kentucky"
	 * searchAdministrativeToponymPath("US.KY.017") = "United-States / Kentucky / Bourbon County"
	 * searchAdministrativeToponymPath("US.FOO")    = "United-States / ?"
	 * </pre>
	 * 
	 * @param administrativeCodePath
	 *            the administrative code path
	 * @return the string
	 */
	public String searchAdministrativeToponymPath(final String administrativeCodePath)
	{
		String result;

		result = this.indexOfAdministrativePaths.searchAdministrativeToponymPath(administrativeCodePath);

		//
		return result;
	}

	/**
	 * Search for the toponym path of an administrative code path.
	 * 
	 * An administrative code path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * Examples of administrative path:
	 * 
	 * <pre>
	 * searchAdministrativeToponymPath("US", null, null, null)  = "United-States"
	 * searchAdministrativeToponymPath("US", "KY", null, null)  = "United-States / Kentucky"
	 * searchAdministrativeToponymPath("US", "KY", "017", null) = "United-States / Kentucky / Bourbon County"
	 * searchAdministrativeToponymPath("US", "FOO", null, null) = "United-States / ?"
	 * </pre>
	 * 
	 * @param countryCode
	 *            the country code
	 * @param adminCode1
	 *            the admin code 1
	 * @param adminCode2
	 *            the admin code 2
	 * @param adminCode3
	 *            the admin code 3
	 * @param adminCode4
	 *            the admin code 4
	 * @return the string
	 */
	public String searchAdministrativeToponymPath(final String countryCode, final String adminCode1, final String adminCode2, final String adminCode3, final String adminCode4)
	{
		String result;

		result = this.indexOfAdministrativePaths.searchAdministrativeToponymPath(countryCode, adminCode1, adminCode2, adminCode3, adminCode4);

		//
		return result;
	}

	/**
	 * Search by administrative path.
	 * 
	 * An administrative path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * @param administrativeCodePath
	 *            the administrative code path
	 * @return the geo names line
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public GeoNamesLine searchByAdministrativeCodePath(final String administrativeCodePath) throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesLine result;

		Integer geonameId = searchAdministrativeGeoNameId(administrativeCodePath);

		if (geonameId == null)
		{
			result = null;
		}
		else
		{
			result = searchByGeoNameId(geonameId);
		}

		//
		return result;
	}

	/**
	 * Search by administrative path.
	 * 
	 * An administrative path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * @param countryCode
	 *            the country code
	 * @param adminCode1
	 *            the admin code 1
	 * @param adminCode2
	 *            the admin code 2
	 * @param adminCode3
	 *            the admin code 3
	 * @param adminCode4
	 *            the admin code 4
	 * @return the geo names line
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public GeoNamesLine searchByAdministrativeCodePath(final String countryCode, final String adminCode1, final String adminCode2, final String adminCode3, final String adminCode4)
			throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesLine result;

		Integer geonameId = searchAdministrativeGeoNameId(countryCode, adminCode1, adminCode2, adminCode3, adminCode4);

		if (geonameId == null)
		{
			result = null;
		}
		else
		{
			result = searchByGeoNameId(geonameId);
		}

		//
		return result;
	}

	/**
	 * Search by geo name id.
	 * 
	 * @param geonameId
	 *            the geoname id
	 * @return the geo names line
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public GeoNamesLine searchByGeoNameId(final int geonameId) throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesLine result;

		String line = this.indexOfIdSeek.get(geonameId);
		result = GeoNamesLineParser.parse(line);

		//
		return result;
	}

	/**
	 * Search for a GeoNamesline by a single input and criteria. Single input
	 * implies AND research for each words.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the geo names lines
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public GeoNamesLines searchBySingleInput(final GeoNamesSearchCriteria criteria) throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesLines result;

		if (criteria == null)
		{
			result = new GeoNamesLines();
		}
		else
		{
			StringList inputWords = IndexOfWordSeeksBuilder.nameToWords(criteria.getInput()).getSelection().toStringList();

			Collections.sort(inputWords, StringLengthComparator.instance());
			Collections.reverse(inputWords);

			logger.debug("inputWords={}", inputWords.toStringWithCommas());

			if (inputWords.isEmpty())
			{
				result = new GeoNamesLines();
			}
			else
			{
				logger.debug("pivot word=" + inputWords.get(0));

				result = searchByWord(inputWords.get(0));

				// Filter by feature class.
				if (!criteria.featureClasses().isEmpty())
				{
					result = result.getByFeatureClasses(criteria.featureClasses());
				}

				//
				if (inputWords.size() > 1)
				{
					GeoNamesLines filteredResult = new GeoNamesLines();

					inputWords.remove(0);

					logger.debug("other input Words=" + inputWords.toStringWithCommas());

					for (GeoNamesLine toponym : result)
					{
						StringSet toponymNames = IndexOfWordSeeksBuilder.lineToNames(toponym);
						StringSet toponymWords = IndexOfWordSeeksBuilder.namesToWords(toponymNames).getSelection();

						if (CollectionUtils.containsAll(toponymWords, inputWords))
						{
							filteredResult.add(toponym);
						}
					}

					result = filteredResult;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Search by word.
	 * 
	 * @param word
	 *            the word
	 * @return the geo names lines
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public GeoNamesLines searchByWord(final String word) throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesLines result;

		StringList lines = this.indexOfWordSeeks.get(word);

		result = GeoNamesLineParser.parse(lines);

		//
		return result;
	}

	/**
	 * Search for the description of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * searchFeatureDescription(null)     = null
	 * searchFeatureDescription("X")      = null
	 * searchFeatureDescription("H")      = "Stream, lake…"
	 * searchFeatureDescription("CNLA")   = "Stream, lake… / a conduit used to carry water"
	 * searchFeatureDescription("H.CNLA") = "Stream, lake… / a conduit used to carry water"
	 * searchFeatureDescription("H.FOO")  = null
	 * </pre>
	 * 
	 * @param featurePath
	 *            the feature path
	 * @return the string
	 */
	public String searchFeatureDescription(final String featurePath)
	{
		String result;

		result = this.indexOfFeatures.getFeatureDescription(featurePath);

		//
		return result;
	}

	/**
	 * Search for the description of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * searchFeatureDescription(null, null)  = null
	 * searchFeatureDescription("X", null)   = null
	 * searchFeatureDescription("H", null)   = "Stream, lake…"
	 * searchFeatureDescription("H", "CNLA") = "a conduit used to carry water"
	 * searchFeatureDescription("H", "FOO")  = null
	 * </pre>
	 * 
	 * @param className
	 *            the class name
	 * @param code
	 *            the code
	 * @return the string
	 */
	public String searchFeatureDescription(final String className, final String code)
	{
		String result;

		result = this.indexOfFeatures.getFeatureDescription(className, code);

		//
		return result;
	}

	/**
	 * Search for the description path of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * searchFeatureDescriptionPath(null)     = null
	 * searchFeatureDescriptionPath("X")      = null
	 * searchFeatureDescriptionPath("H")      = "Stream, lake…"
	 * searchFeatureDescriptionPath("H.FOO")  = "Stream, lake… / ?"
	 * searchFeatureDescriptionPath("CNLA")   = "Stream, lake… / a conduit used to carry water"
	 * searchFeatureDescriptionPath("H.CNLA") = "Stream, lake… / a conduit used to carry water"
	 * </pre>
	 * 
	 * @param codePath
	 *            the code path
	 * @return the string
	 */
	public String searchFeatureDescriptionPath(final String codePath)
	{
		String result;

		result = this.indexOfFeatures.getFeatureDescriptionPath(codePath);

		//
		return result;
	}

	/**
	 * Search for the description path of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * searchFeatureDescriptionPath(null, null)  = null
	 * searchFeatureDescriptionPath("X", null)   = null
	 * searchFeatureDescriptionPath("H", "null") = "Stream, lake…"
	 * searchFeatureDescriptionPath("H", "FOO")  = "Stream, lake… / ?"
	 * searchFeatureDescriptionPath("H", "CNLA") = "Stream, lake… / a conduit used to carry water"
	 * </pre>
	 * 
	 * @param className
	 *            the class name
	 * @param code
	 *            the code
	 * @return the string
	 */
	public String searchFeatureDescriptionPath(final String className, final String code)
	{
		String result;

		result = this.indexOfFeatures.getFeatureDescriptionPath(className, code);

		//
		return result;
	}

	/**
	 * Search for the short description of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * searchFeatureShortDescription(null)     = null
	 * searchFeatureShortDescription("X")      = null
	 * searchFeatureShortDescription("H")      = "Stream, lake…"
	 * searchFeatureShortDescription("CNLA")   = "aqueduct"
	 * searchFeatureShortDescription("H.CNLA") = "aqueduct"
	 * searchFeatureShortDescription("H.FOO")  = null
	 * </pre>
	 * 
	 * @param codePath
	 *            the code path
	 * @return the string
	 */
	public String searchFeatureShortDescription(final String codePath)
	{
		String result;

		result = this.indexOfFeatures.getFeatureShortDescription(codePath);

		//
		return result;
	}

	/**
	 * Search for the short description of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * searchFeatureShortDescription(null, null)  = null
	 * searchFeatureShortDescription("X", null)   = null
	 * searchFeatureShortDescription("H", null)   = "Stream, lake…"
	 * searchFeatureShortDescription("H", "CNLA") = "aqueduct"
	 * searchFeatureShortDescription("H", "FOO")  = null
	 * </pre>
	 * 
	 * @param className
	 *            the class name
	 * @param code
	 *            the code
	 * @return the string
	 */
	public String searchFeatureShortDescription(final String className, final String code)
	{
		String result;

		result = this.indexOfFeatures.getFeatureShortDescription(className, code);

		//
		return result;
	}

	/**
	 * Search raw line by geo name id.
	 * 
	 * @param geonameId
	 *            the geoname id
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String searchRawLineByGeoNameId(final int geonameId) throws IOException
	{
		String result;

		result = this.indexOfIdSeek.get(geonameId);

		//
		return result;
	}

	/**
	 * Search raw line by geo name id.
	 * 
	 * @param geonameId
	 *            the geoname id
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String searchRawLineByGeoNameId(final String geonameId) throws IOException
	{
		String result;

		if (NumberUtils.isDigits(geonameId))
		{
			result = searchRawLineByGeoNameId(Integer.parseInt(geonameId));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Search raw line by word.
	 * 
	 * @param word
	 *            the word
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public StringList searchRawLineByWord(final String word) throws IOException
	{
		StringList result;

		result = this.indexOfWordSeeks.get(word);

		//
		return result;
	}
}
