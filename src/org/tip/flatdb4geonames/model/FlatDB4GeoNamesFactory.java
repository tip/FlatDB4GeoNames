/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.index.IndexOfIdSeekBuilder;
import org.tip.flatdb4geonames.model.index.IndexOfWordSeeksBuilder;
import org.tip.flatdb4geonames.util.Downloader;
import org.tip.flatdb4geonames.util.Unzipper;

/**
 * The Class FlatDB4GeoNamesFactory is a factory of the FlatDB4GeoNames project.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class FlatDB4GeoNamesFactory
{
	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNamesFactory.class);

	public static final String GEONAMES_DUMP_REPOSITORY = "http://download.geonames.org/export/dump/";

	public static final String DEFAULT_DATABASE_FILE = "FlatDB4GeoNames-database.zip";

	/**
	 * Builds the index.
	 * 
	 * @param localRepository
	 *            the local repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void buildIndex(final File localRepository) throws IOException
	{
		logger.debug("Build index start...");

		File geonamesFile = new File(localRepository.getAbsoluteFile() + File.separator + GeoNamesFlatDatabase.GEONAMES_MAIN_FILENAME);

		//
		IndexOfWordSeeksBuilder.buildIndex(geonamesFile, localRepository, IndexOfWordSeeksBuilder.DEFAULT_SPLIT_LINE, IndexOfWordSeeksBuilder.DEFAULT_PADDING_LENGTH);

		IndexOfIdSeekBuilder.buildIndex(geonamesFile, localRepository, IndexOfIdSeekBuilder.DEFAULT_SPLIT_LINE, IndexOfIdSeekBuilder.DEFAULT_PADDING_LENGTH);

		logger.debug("Build index done.");
	}

	/**
	 * Close.
	 * 
	 * @param database
	 *            the database
	 */
	public static void close(final GeoNamesFlatDatabase database)
	{
		if (database != null)
		{
			database.close();
		}
	}

	/**
	 * Creates the.
	 * 
	 * @param localTargetDirectory
	 *            the local target directory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void create(final File localTargetDirectory) throws IOException
	{
		//
		downloadGeoNamesFiles(localTargetDirectory);

		//
		buildIndex(localTargetDirectory);
	}

	/**
	 * Download database.
	 * 
	 * @param sourceRepositoryUrl
	 *            the source repository url
	 * @param localTargetRepository
	 *            the local target repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void downloadDatabase(final String sourceRepositoryUrl, final File localTargetRepository) throws IOException
	{
		if ((StringUtils.isBlank(sourceRepositoryUrl)) || (localTargetRepository == null))
		{
			throw new IllegalArgumentException("Null parameter detected.");
		}
		else
		{
			String sourceUrl;
			if (sourceRepositoryUrl.endsWith("/"))
			{
				sourceUrl = sourceRepositoryUrl;
			}
			else
			{
				sourceUrl = sourceRepositoryUrl + "/";
			}

			//
			File target = Downloader.download(sourceUrl + DEFAULT_DATABASE_FILE, localTargetRepository);
			Unzipper.unzip(target, localTargetRepository);
			boolean deleteStatus = target.delete();
			if (!deleteStatus)
			{
				logger.error("Failed to delete file [{}]", target.getAbsolutePath());
			}
		}
	}

	/**
	 * Downloads required files from the official GeoNames repository to a local
	 * repository.
	 * 
	 * @param localTargetRepository
	 *            the local target repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void downloadGeoNamesFiles(final File localTargetRepository) throws IOException
	{
		downloadGeoNamesFiles(GEONAMES_DUMP_REPOSITORY, localTargetRepository);
	}

	/**
	 * Download geo names files.
	 * 
	 * @param sourceRepositoryUrl
	 *            the source repository url
	 * @param localTargetRepository
	 *            the local target repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void downloadGeoNamesFiles(final String sourceRepositoryUrl, final File localTargetRepository) throws IOException
	{
		if ((StringUtils.isBlank(sourceRepositoryUrl)) || (localTargetRepository == null))
		{
			throw new IllegalArgumentException("Null parameter detected.");
		}
		else if (!localTargetRepository.isDirectory())
		{
			throw new IllegalArgumentException("Local target repository is not a directory.");
		}
		else
		{
			String sourceUrl;
			if (sourceRepositoryUrl.endsWith("/"))
			{
				sourceUrl = sourceRepositoryUrl;
			}
			else
			{
				sourceUrl = sourceRepositoryUrl + "/";
			}

			//
			File target = Downloader.download(sourceUrl + "allCountries.zip", localTargetRepository);
			Unzipper.unzip(target, localTargetRepository);
			boolean deleteStatus = target.delete();
			if (!deleteStatus)
			{
				logger.error("Failed to delete file [{}]", target.getAbsolutePath());
			}

			//
			Downloader.download(sourceUrl + GeoNamesFlatDatabase.GEONAMES_FEATURES_FILE, localTargetRepository);

			//
			Downloader.download(sourceUrl + GeoNamesFlatDatabase.GEONAMES_ADMINISTRATIVE_CODE1_FILE, localTargetRepository);

			//
			Downloader.download(sourceUrl + GeoNamesFlatDatabase.GEONAMES_ADMINISTRATIVE_CODE2_FILE, localTargetRepository);

			//
			FileUtils.copyURLToFile(FlatDB4GeoNamesFactory.class.getResource("/org/tip/flatdb4geonames/model/ressources/README"), new File(localTargetRepository, "README"));
			FileUtils.copyURLToFile(FlatDB4GeoNamesFactory.class.getResource("/org/tip/flatdb4geonames/model/ressources/LICENSE"), new File(localTargetRepository, "LICENSE"));
		}
	}

	/**
	 * Checks if is valid database.
	 * 
	 * @param directory
	 *            the directory
	 * @return true, if is valid database
	 */
	public static boolean isValidDatabase(final File directory)
	{
		boolean result;

		if (directory == null)
		{
			result = false;
		}
		else if (!directory.isDirectory())
		{
			result = false;
		}
		else if (!directory.exists())
		{
			result = false;
		}
		else
		{
			File mainFile = new File(directory.getAbsoluteFile() + File.separator + GeoNamesFlatDatabase.GEONAMES_MAIN_FILENAME);
			File featuresFile = new File(directory.getAbsoluteFile() + File.separator + GeoNamesFlatDatabase.GEONAMES_FEATURES_FILE);
			File index1File = new File(directory.getAbsoluteFile() + File.separator + IndexOfIdSeekBuilder.DEFAULT_INDEX_FILENAME);
			File index2File = new File(directory.getAbsoluteFile() + File.separator + IndexOfIdSeekBuilder.DEFAULT_INDEX_FILENAME);
			File index3File = new File(directory.getAbsoluteFile() + File.separator + IndexOfWordSeeksBuilder.INDEX1_FILENAME);
			File index4File = new File(directory.getAbsoluteFile() + File.separator + IndexOfWordSeeksBuilder.INDEX2_FILENAME);
			File index5File = new File(directory.getAbsoluteFile() + File.separator + GeoNamesFlatDatabase.GEONAMES_ADMINISTRATIVE_CODE2_FILE);

			if ((!mainFile.exists()) || (!mainFile.isFile()))
			{
				logger.debug("Missing :" + mainFile.getAbsolutePath());
				result = false;
			}
			else if ((!featuresFile.exists()) || (!featuresFile.isFile()))
			{
				logger.debug("Missing :" + featuresFile.getAbsolutePath());
				result = false;
			}
			else if ((!index1File.exists()) || (!index1File.isFile()))
			{
				logger.debug("Missing :" + index1File.getAbsolutePath());
				result = false;
			}
			else if ((!index2File.exists()) || (!index2File.isFile()))
			{
				logger.debug("Missing :" + index2File.getAbsolutePath());
				result = false;
			}
			else if ((!index3File.exists()) || (!index3File.isFile()))
			{
				logger.debug("Missing :" + index3File.getAbsolutePath());
				result = false;
			}
			else if ((!index4File.exists()) || (!index4File.isFile()))
			{
				logger.debug("Missing :" + index4File.getAbsolutePath());
				result = false;
			}
			else if ((!index5File.exists()) || (!index5File.isFile()))
			{
				logger.debug("Missing :" + index4File.getAbsolutePath());
				result = false;
			}
			else
			{
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * Open.
	 * 
	 * @param repository
	 *            the repository
	 * @return the geo names flat database
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static GeoNamesFlatDatabase open(final File repository) throws IOException
	{
		GeoNamesFlatDatabase result;

		if (repository == null)
		{
			throw new IllegalArgumentException("Null parameter detected.");
		}
		else
		{
			result = new GeoNamesFlatDatabase(repository);
		}

		//
		return result;
	}

	/**
	 * Open.
	 * 
	 * @param repository
	 *            the repository
	 * @return the geo names flat database
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static GeoNamesFlatDatabase open(final String repository) throws IOException
	{
		GeoNamesFlatDatabase result;

		result = open(new File(repository));

		//
		return result;
	}
}
