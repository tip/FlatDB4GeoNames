/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

/**
 * The Class FlatDB4GeoNamesException.
 */
public class FlatDB4GeoNamesException extends Exception
{
	private static final long serialVersionUID = -1744751404874861275L;

	/**
	 * Instantiates a new flat DB 4 geo names exception.
	 */
	public FlatDB4GeoNamesException()
	{
		super();
	}

	/**
	 * Instantiates a new flat DB 4 geo names exception.
	 * 
	 * @param message
	 *            the message
	 */
	public FlatDB4GeoNamesException(final String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new flat DB 4 geo names exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public FlatDB4GeoNamesException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Instantiates a new flat DB 4 geo names exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public FlatDB4GeoNamesException(final Throwable cause)
	{
		super(cause);
	}
}
