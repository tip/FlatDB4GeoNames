/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import org.tip.flatdb4geonames.model.index.FeatureClasses;

/**
 * The Class GeoNamesSearchCriteria.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeoNamesSearchCriteria
{
	public enum Continent
	{
		ALL("*"),
		AFRICA("AF"),
		ANTARCTICA("AN"),
		ASIA("AS"),
		EUROPE("EU"),
		NORTH_AMERICA("NA"),
		OCEANIA("OC"),
		SOUTH_AMERICA("SA");

		private String code;

		/**
		 * Instantiates a new continent.
		 * 
		 * @param code
		 *            the code
		 */
		private Continent(final String code)
		{
			this.code = code;
		}

		/**
		 * Gets the code.
		 * 
		 * @return the code
		 */
		public String getCode()
		{
			return this.code;
		}

		/**
		 * Value of code.
		 * 
		 * @param code
		 *            the code
		 * @return the continent
		 */
		public Continent valueOfCode(final String code)
		{
			Continent result;

			if (code == null)
			{
				result = ALL;
			}
			else if (code.equals("AF"))
			{
				result = AFRICA;
			}
			else if (code.equals("AN"))
			{
				result = ANTARCTICA;
			}
			else if (code.equals("AS"))
			{
				result = ASIA;
			}
			else if (code.equals("EU"))
			{
				result = EUROPE;
			}
			else if (code.equals("NA"))
			{
				result = NORTH_AMERICA;
			}
			else if (code.equals("OC"))
			{
				result = OCEANIA;
			}
			else if (code.equals("SA"))
			{
				result = SOUTH_AMERICA;
			}
			else
			{
				result = ALL;
			}

			//
			return result;
		}
	}

	public enum SearchMode
	{
		PART_MATCH,
		FULL_MATCH
	}

	// ////////////////////////////////////

	private SearchMode searchMode;
	private Continent continentScope;
	private String countryCode;
	private FeatureClasses featureClasses;
	private String input;

	/**
	 * Instantiates a new geo names search criteria.
	 */
	public GeoNamesSearchCriteria()
	{
		this.searchMode = SearchMode.PART_MATCH;
		this.continentScope = Continent.ALL;
		this.countryCode = null;
		this.featureClasses = new FeatureClasses();
	}

	/**
	 * Instantiates a new geo names search criteria.
	 * 
	 * @param source
	 *            the source
	 */
	public GeoNamesSearchCriteria(final GeoNamesSearchCriteria source)
	{
		this.searchMode = source.getSearchMode();
		this.continentScope = source.getContinentScope();
		this.countryCode = source.getCountryCode();
		this.featureClasses = source.featureClasses();
		this.input = source.getInput();
	}

	/**
	 * Feature classes.
	 * 
	 * @return the feature classes
	 */
	public FeatureClasses featureClasses()
	{
		return this.featureClasses;
	}

	/**
	 * Gets the continent scope.
	 * 
	 * @return the continent scope
	 */
	public Continent getContinentScope()
	{
		return this.continentScope;
	}

	/**
	 * Gets the country code.
	 * 
	 * @return the country code
	 */
	public String getCountryCode()
	{
		return this.countryCode;
	}

	/**
	 * Gets the input.
	 * 
	 * @return the input
	 */
	public String getInput()
	{
		return this.input;
	}

	/**
	 * Gets the search mode.
	 * 
	 * @return the search mode
	 */
	public SearchMode getSearchMode()
	{
		return this.searchMode;
	}

	/**
	 * Sets the continent scope.
	 * 
	 * @param continentScope
	 *            the new continent scope
	 */
	public void setContinentScope(final Continent continentScope)
	{
		this.continentScope = continentScope;
	}

	/**
	 * Sets the country code.
	 * 
	 * @param countryCode
	 *            the new country code
	 */
	public void setCountryCode(final String countryCode)
	{
		this.countryCode = countryCode;
	}

	/**
	 * Sets the input.
	 * 
	 * @param input
	 *            the new input
	 */
	public void setInput(final String input)
	{
		this.input = input;
	}

	/**
	 * Sets the search mode.
	 * 
	 * @param searchMode
	 *            the new search mode
	 */
	public void setSearchMode(final SearchMode searchMode)
	{
		this.searchMode = searchMode;
	}
}
