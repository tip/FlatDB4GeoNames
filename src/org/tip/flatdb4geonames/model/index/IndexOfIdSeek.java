/*
 * Copyright (C) 2015-2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class IndexOfIdSeek
{
	private static Logger logger = LoggerFactory.getLogger(IndexOfIdSeek.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private RandomAccessFile data;
	private RandomAccessFile index;
	private int lineLength;

	/**
	 * 
	 * @param dataFile
	 * @param indexFile
	 * 
	 * @throws IOException
	 */
	public IndexOfIdSeek(final File dataFile, final File indexDirectory) throws IOException
	{
		if ((dataFile == null) || (indexDirectory == null))
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.data = new RandomAccessFile(dataFile, "r");

			File indexFile = new File(indexDirectory.getAbsoluteFile() + File.separator + IndexOfIdSeekBuilder.DEFAULT_INDEX_FILENAME);
			this.index = new RandomAccessFile(indexFile, "r");

			// Compute the line length of the index file for future seek
			// computations.
			BufferedReader in = null;
			try
			{
				in = new BufferedReader(new InputStreamReader(new FileInputStream(indexFile), DEFAULT_CHARSET_NAME));
				this.lineLength = in.readLine().length() + 1;
				logger.debug("index1.lineLength={}", this.lineLength);
			}
			finally
			{
				IOUtils.closeQuietly(in);
			}
		}
	}

	/**
	 * 
	 */
	public void close()
	{
		IOUtils.closeQuietly(this.data);
		IOUtils.closeQuietly(this.index);
	}

	/**
	 * 
	 * @param geonameId
	 * @return
	 * @throws IOException
	 */
	public String get(final int geonameId) throws IOException
	{
		String result;

		if (geonameId < 1)
		{
			throw new IllegalArgumentException("Bad id.");
		}
		else
		{
			long indexSeek = (geonameId - 1) * this.lineLength;

			this.index.seek(indexSeek);
			String indexLine = this.index.readLine();

			logger.debug("get({})=seek({})=[{}]", geonameId, indexSeek, indexLine);

			if (indexLine.charAt(0) == ' ')
			{
				result = null;
			}
			else
			{
				long dataSeek = Long.parseLong(indexLine.trim());

				this.data.seek(dataSeek);

				result = IndexOfWordSeeks.readUTFLine(this.data);
			}
		}

		//
		return result;
	}
}
