/*
 * Copyright (C) 2015-2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import fr.devinsy.util.StringSet;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class WordTriage
{
	private StringSet selection;
	private StringSet rejection;

	/**
	 * 
	 */
	public WordTriage()
	{
		this.selection = new StringSet();
		this.rejection = new StringSet();
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final WordTriage source)
	{
		this.selection.addAll(source.getSelection());
		this.rejection.addAll(source.getRejection());
	}

	public StringSet getRejection()
	{
		return this.rejection;
	}

	public StringSet getSelection()
	{
		return this.selection;
	}
}
