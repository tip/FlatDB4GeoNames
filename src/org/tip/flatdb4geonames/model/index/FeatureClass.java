/*
 * Copyright (C) 2015-2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public enum FeatureClass
{
	COUNTRY_STATE_REGION("A", "Country, state, region…"),
	STREAM_LAKE("H", "Stream, lake…"),
	PARK_AREA("L", "Parks,area…"),
	CITY_VILLAGE("P", "City, village…"),
	ROAD_RAILROAD("R", "Road, railroad"),
	SPOT_BUILDING_FARM("S", "Spot, building, farm"),
	MOUNTAIN_HILL_ROCK("T", "Mountain, hill, rock…"),
	UNDERSEA("U", "Undersea"),
	FOREST_HEATH("V", "Forest,heath…");

	private String code;
	private String description;

	/**
	 * 
	 * @param code
	 */
	private FeatureClass(final String code, final String description)
	{
		this.code = code;
		this.description = description;
	}

	/**
	 * 
	 * @return
	 */
	public String getCode()
	{
		return this.code;
	}

	public String getDescription()
	{
		return this.description;
	}

	/**
	 * 
	 * @param code
	 * @return
	 */
	public static boolean isClassCode(final String code)
	{
		boolean result;

		if ((code != null) && (code.length() == 1) && (StringUtils.contains("AHLPRSTUV", code)))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final FeatureClass source)
	{
		String result;

		if (source == null)
		{
			result = "";
		}
		else
		{
			result = source.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param code
	 * @return
	 */
	public static FeatureClass valueOfCode(final String code)
	{
		FeatureClass result;

		if (code == null)
		{
			result = null;
		}
		else if (code.equals("A"))
		{
			result = COUNTRY_STATE_REGION;
		}
		else if (code.equals("H"))
		{
			result = STREAM_LAKE;
		}
		else if (code.equals("L"))
		{
			result = PARK_AREA;
		}
		else if (code.equals("P"))
		{
			result = CITY_VILLAGE;
		}
		else if (code.equals("R"))
		{
			result = ROAD_RAILROAD;
		}
		else if (code.equals("S"))
		{
			result = SPOT_BUILDING_FARM;
		}
		else if (code.equals("T"))
		{
			result = MOUNTAIN_HILL_ROCK;
		}
		else if (code.equals("U"))
		{
			result = UNDERSEA;
		}
		else if (code.equals("V"))
		{
			result = FOREST_HEATH;
		}
		else
		{
			result = null;
		}

		//
		return result;
	}
}
