/*
 * Copyright (C) 2015-2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class IndexOfFeatures
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(IndexOfFeatures.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private Features features;
	private Map<String, Feature> index;

	/**
	 * @throws IOException
	 * 
	 */
	public IndexOfFeatures(final File source) throws IOException
	{
		//
		this.features = new Features();
		this.index = new HashMap<String, Feature>();

		//
		for (FeatureClass featureClass : FeatureClass.values())
		{
			Feature feature = new Feature(featureClass.getCode(), null, featureClass.getDescription(), null);
			this.features.add(feature);
			this.index.put(feature.getCodePath(), feature);
		}

		//
		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));

			int lineCount = 0;
			boolean ended = false;
			while (!ended)
			{
				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					// logger.debug(line);
					lineCount += 1;

					String[] tokens = line.split("\t");

					String[] tags = tokens[0].split("\\.");

					if (tags.length > 1)
					{
						String featureClassCode = tags[0];
						String featureCode = tags[1];

						String shortDescription = tokens[1];

						String description;
						if (tokens.length > 2)
						{
							description = tokens[2];
						}
						else
						{
							description = null;
						}

						Feature feature = new Feature(featureClassCode, featureCode, shortDescription, description);

						this.features.add(feature);
						this.index.put(feature.getCodePath(), feature);
						this.index.put(feature.getFeatureCode(), feature);

						// logger.debug(feature.toString());
					}
				}
			}

			logger.debug("count line=" + lineCount);
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.index.clear();
		this.features.clear();
	}

	/**
	 * 
	 * <pre>
	 * getFeatureDescription(null)     = null
	 * getFeatureDescription("X")      = null
	 * getFeatureDescription("H")      = Object Feature H
	 * getFeatureDescription("CNLA")   = Object Feature H.CNLA
	 * getFeatureDescription("H.CNLA") = Object Feature H.CNLA
	 * getFeatureDescription("H.FOO")  = null
	 * </pre>
	 * 
	 * @param className
	 * @param code
	 * @return
	 */
	public Feature get(final String featurePath)
	{
		Feature result;

		result = this.index.get(featurePath);

		//
		return result;
	}

	/**
	 * 
	 * <pre>
	 * getFeatureDescription(null, null)  = null
	 * getFeatureDescription("X", null)   = null
	 * getFeatureDescription("H", null)   = Feature H
	 * getFeatureDescription("H", "CNLA") = Feature H.CNLA
	 * getFeatureDescription("H", "FOO")  = null
	 * </pre>
	 * 
	 * @param className
	 * @param code
	 * @return
	 */
	public Feature get(final String className, final String code)
	{
		Feature result;

		result = this.index.get(Feature.convertToFeaturePath(className, code));

		//
		return result;
	}

	/**
	 * Returns the description of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * getFeatureDescription(null)     = null
	 * getFeatureDescription("X")      = null
	 * getFeatureDescription("H")      = "Stream, lake…"
	 * getFeatureDescription("CNLA")   = "a conduit used to carry water"
	 * getFeatureDescription("H.CNLA") = "a conduit used to carry water"
	 * getFeatureDescription("H.FOO")  = null
	 * </pre>
	 * 
	 * @param featurePath
	 * @return
	 */
	public String getFeatureDescription(final String featurePath)
	{
		String result;

		Feature feature = get(featurePath);

		if (feature == null)
		{
			result = null;
		}
		else
		{
			result = feature.getDescription();
		}

		//
		return result;
	}

	/**
	 * Returns the description of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * getFeatureDescription(null, null)   = null
	 * getFeatureDescription("X", null)    = null
	 * getFeatureDescription("H", null)    = "Stream, lake…"
	 * getFeatureDescription("H", "CNLA")  = "Stream, lake… / a conduit used to carry water"
	 * getFeatureDescription("H", "FOO")   = null
	 * getFeatureDescription(null, "CNLA") = "a conduit used to carry water"
	 * getFeatureDescription(null, "FOO")  = null
	 * </pre>
	 * 
	 * @param className
	 * @param code
	 * @return
	 */
	public String getFeatureDescription(final String className, final String code)
	{
		String result;

		result = getFeatureDescription(Feature.convertToFeaturePath(className, code));

		//
		return result;
	}

	/**
	 * Returns the description path of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * getFeatureDescriptionPath(null)     = null
	 * getFeatureDescriptionPath("X")      = null
	 * getFeatureDescriptionPath("H")      = "Stream, lake…"
	 * getFeatureDescriptionPath("CNLA")   = "Stream, lake… / a conduit used to carry water"
	 * getFeatureDescriptionPath("H.CNLA") = "Stream, lake… / a conduit used to carry water"
	 * getFeatureDescriptionPath("H.FOO")  = "Stream, lake… / ?"
	 * </pre>
	 * 
	 * @param codePath
	 * @return
	 */
	public String getFeatureDescriptionPath(final String codePath)
	{
		String result;

		Feature feature = get(codePath);

		if (feature == null)
		{
			result = null;
		}
		else
		{
			result = feature.getDescriptionPath();
		}

		//
		return result;
	}

	/**
	 * Returns the description path of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * getFeatureShortDescriptionPath(null, null)  = null
	 * getFeatureShortDescriptionPath("X", null)   = null
	 * getFeatureShortDescriptionPath("H", "null") = "Stream, lake…"
	 * getFeatureShortDescriptionPath("H", "CNLA") = "Stream, lake… / aqueduct"
	 * getFeatureShortDescriptionPath("H", "FOO")  = "Stream, lake… / ?"
	 * </pre>
	 * 
	 * @param featureClassName
	 * @param featureCode
	 * @return
	 */
	public String getFeatureDescriptionPath(final String featureClassName, final String featureCode)
	{
		String result;

		Feature feature = get(featureClassName, featureCode);

		if (feature == null)
		{
			result = null;
		}
		else
		{
			result = feature.getDescriptionPath();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Features getFeatures()
	{
		Features result;

		result = new Features(this.features);

		//
		return result;
	}

	/**
	 * Returns the short description of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * getFeatureShortDescription(null)     = null
	 * getFeatureShortDescription("X")      = null
	 * getFeatureShortDescription("H")      = "Stream, lake…"
	 * getFeatureShortDescription("H.CNLA") = "aqueduct"
	 * getFeatureShortDescription("H.FOO")  = null
	 * </pre>
	 * 
	 * @param code
	 * @return
	 */
	public String getFeatureShortDescription(final String featurePath)
	{
		String result;

		Feature feature = get(featurePath);

		if (feature == null)
		{
			result = null;
		}
		else
		{
			result = feature.getShortDescription();
		}

		//
		return result;
	}

	/**
	 * Returns the short description of a feature path.
	 * 
	 * An feature path is a concatenation of feature codes separated by point:
	 * FeatureClass.FeatureCode
	 * 
	 * <pre>
	 * getFeatureShortDescription(null, null)  = null
	 * getFeatureShortDescription("X", null)   = null
	 * getFeatureShortDescription("H", null)   = "Stream, lake…"
	 * getFeatureShortDescription("H", "CNLA") = "aqueduct"
	 * getFeatureShortDescription("H", "FOO")  = null
	 * </pre>
	 * 
	 * @param className
	 * @param code
	 * @return
	 */
	public String getFeatureShortDescription(final String className, final String code)
	{
		String result;

		result = getFeatureShortDescription(Feature.convertToFeaturePath(className, code));

		//
		return result;
	}
}
