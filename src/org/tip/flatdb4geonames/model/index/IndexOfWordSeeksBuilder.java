/*
 * Copyright (C) 2015-2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.GeoNamesLine;
import org.tip.flatdb4geonames.util.Chronometer;
import org.tip.flatdb4geonames.util.Shrinker;
import org.tip.flatdb4geonames.util.StringFileSorter;

import fr.devinsy.util.StringSet;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class IndexOfWordSeeksBuilder
{
	private static Logger logger = LoggerFactory.getLogger(IndexOfWordSeeksBuilder.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final String INDEX0_FILENAME = "word_seeks.index";
	public static final String INDEX1_FILENAME = "seeks.index";
	public static final String INDEX2_FILENAME = "word_seek.index";

	public static final int DEFAULT_SPLIT_LINE = 2000000;
	public static final int WORD_MAX_LENGTH = 30;

	// About the line padding length :
	// seeks file size is ~300 000 000 B => 9 => an upper bound is
	// 10
	public static final int DEFAULT_PADDING_LENGTH = WORD_MAX_LENGTH + 10;

	// '・' is word separator in Katakanas (Japonais)
	public static final String SEPARATOR_CHARACTERS = " ・+*-_()?'’‘`”|\\[\\]!«»%{}\\:;/\\.#@&\"";

	/**
	 * 
	 * @param geonamesFile
	 *            allCountries.txt file
	 * 
	 * @param outputFile
	 * @param splitLimit
	 * 
	 * @throws IOException
	 */
	public static void buildIndex(final File geonamesFile, final File outputDirectory) throws IOException
	{
		buildIndex(geonamesFile, outputDirectory, DEFAULT_SPLIT_LINE, DEFAULT_PADDING_LENGTH);
	}

	/**
	 * 
	 * @param geonamesFile
	 *            allCountries.txt file
	 * 
	 * @param outputFile
	 * @param splitLimit
	 * 
	 * @throws IOException
	 */
	public static void buildIndex(final File geonamesFile, final File outputDirectory, final int splitLimit, final int paddingLength) throws IOException
	{
		if ((geonamesFile == null) || (outputDirectory == null))
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else if (!outputDirectory.isDirectory())
		{
			throw new IllegalArgumentException("Output directory is not a directory.");
		}
		else if (splitLimit < 100000)
		{
			throw new IllegalArgumentException("Invalid split value [" + splitLimit + "]");
		}
		else
		{
			logger.debug("build index start...");

			//
			logger.debug("currentDirectory={}", new File(".").getAbsolutePath());
			logger.debug("Max   memory= {} Mo", Runtime.getRuntime().maxMemory() / 1024 / 1024);
			logger.debug("Total memory= {} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);

			//
			File index0File = buildIndex0(geonamesFile, outputDirectory, splitLimit);

			buildIndex12(index0File, outputDirectory, splitLimit, paddingLength);

			boolean deleteStatus = index0File.delete();
			if (!deleteStatus)
			{
				logger.error("Failed to delete file [{}]", index0File.getAbsolutePath());
			}

			// File index2File = new File(outputDirectory.getAbsoluteFile() +
			// File.separator + INDEX2_FILENAME);
			// buildIndex2(index0File, index2File);

			logger.debug("build index done.");
		}
	}

	/**
	 * 
	 * @param geonamesFile
	 *            allCountries.txt file
	 * 
	 * @param outputFile
	 * @param splitLimit
	 * 
	 * @throws IOException
	 */
	public static File buildIndex0(final File geonamesFile, final File outputDirectory, final int splitLimit) throws IOException
	{
		File result;

		if ((geonamesFile == null) || (outputDirectory == null))
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else if (!outputDirectory.isDirectory())
		{
			throw new IllegalArgumentException("Output directory is not a directory.");
		}
		else if (splitLimit < 100000)
		{
			throw new IllegalArgumentException("Invalid split value [" + splitLimit + "]");
		}
		else
		{
			logger.debug("build index0 file start...");
			Chronometer chrono = new Chronometer();

			//
			logger.debug("geonames file to word seek file start...");
			File index0File = new File(outputDirectory.getAbsoluteFile() + File.separator + INDEX0_FILENAME);
			Chronometer chronoStep = new Chronometer();
			geonamesToWordSeek(geonamesFile, index0File);
			logger.debug("geonames file to word seek file done. {}", chronoStep.stop().interval());

			System.gc();
			logger.debug(" memory={} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);

			//
			logger.debug("sort big string file start...");
			chronoStep.reset();
			StringFileSorter.sortBigStringFile(index0File, splitLimit);
			logger.debug("sort big string file done. {}", chronoStep.stop().interval());

			System.gc();
			logger.debug(" memory={} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);

			//
			logger.debug("shrink string file start...");
			chronoStep.reset();
			Shrinker.shrinkStringFile(index0File);
			logger.debug("shrink string file done. {}", chrono.stop().interval());

			System.gc();
			logger.debug(" memory={} Mo", Runtime.getRuntime().totalMemory() / 1024 / 1024);

			//
			logger.debug("build index0 file done.");

			//
			result = index0File;
		}

		//
		return result;
	}

	/**
	 * @throws IOException
	 * 
	 */
	public static void buildIndex12(final File source, final File outputDirectory, final int splitLimit, final int paddingLength) throws IOException
	{
		logger.debug("build index12 files start...");
		Chronometer chronoStep = new Chronometer();

		long currentSeekValue = 0;
		BufferedReader in = null;
		PrintWriter out1 = null;
		PrintWriter out2 = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));

			File index1File = new File(outputDirectory.getAbsoluteFile() + File.separator + INDEX1_FILENAME);
			out1 = new PrintWriter(index1File, DEFAULT_CHARSET_NAME);

			File index2File = new File(outputDirectory.getAbsoluteFile() + File.separator + INDEX2_FILENAME);
			out2 = new PrintWriter(index2File, DEFAULT_CHARSET_NAME);

			int lineCount = 0;
			boolean ended = false;
			while (!ended)
			{
				if (lineCount % 1000000 == 0)
				{
					System.gc();

					logger.debug("\tlineCount=" + lineCount + "\tcurrentSeekValue=" + currentSeekValue + " \tmemory=" + Runtime.getRuntime().totalMemory() / 1024 / 1024 + " Mo \t"
							+ ((int) (chronoStep.stop().interval() / 1000)) + " s");
					chronoStep.reset();
				}

				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					lineCount += 1;

					String[] tokens = line.split("\t");
					String word = tokens[0];
					String seeks = tokens[1];

					out1.println(seeks);

					String data = word + "\t" + currentSeekValue;
					out2.println(data + StringUtils.repeat(" ", paddingLength - data.getBytes().length));

					currentSeekValue += seeks.getBytes().length + 1;
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out1);
			IOUtils.closeQuietly(out2);
		}

		logger.debug("build index12 files done.");
	}

	/**
	 * Returns all words without filter on the size or starting characters.
	 * 
	 * @param line
	 * @return
	 */
	public static StringSet geoNameLineToRawWords(final String line)
	{
		StringSet result;

		StringSet names = lineToNames(line);
		result = namesToRawWords(names);

		//
		return result;
	}

	/**
	 * 
	 * @param line
	 * @return
	 */
	public static WordTriage geoNameLineToWords(final String line)
	{
		WordTriage result;

		StringSet names = lineToNames(line);
		result = namesToWords(names);

		//
		return result;
	}

	/**
	 * @throws IOException
	 * 
	 */
	public static void geonamesToWordSeek(final File source, final File target) throws IOException
	{
		Chronometer chronoStep = new Chronometer();

		long currentSeekValue = 0;
		BufferedReader in = null;
		PrintWriter out = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(target), DEFAULT_CHARSET_NAME));

			int lineCount = 0;
			boolean ended = false;
			StringSet rejectedWords = new StringSet();
			while (!ended)
			{
				if (lineCount % 1000000 == 0)
				{
					System.gc();

					logger.debug("\tlineCount=" + lineCount + "\tcurrentSeekValue=" + currentSeekValue + " \tmemory=" + Runtime.getRuntime().totalMemory() / 1024 / 1024 + " Mo \t"
							+ ((int) (chronoStep.stop().interval() / 1000)) + " s");
					chronoStep.reset();
				}

				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					lineCount += 1;

					WordTriage words = geoNameLineToWords(line);

					for (String word : words.getSelection())
					{
						out.println(word + "\t" + currentSeekValue);
					}

					rejectedWords.addAll(words.getRejection());

					currentSeekValue += line.getBytes().length + 1;
				}
			}

			//
			FileUtils.writeLines(new File(target.getAbsolutePath() + ".rejection"), rejectedWords);
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringSet lineToNames(final GeoNamesLine line)
	{
		StringSet result;

		result = new StringSet();

		String name = line.getName();
		if (StringUtils.isNotBlank(name))
		{
			result.add(name);
		}

		String asciiName = line.getAsciiName();
		if (StringUtils.isNotBlank(asciiName))
		{
			result.add(asciiName);
		}

		for (String alternateName : line.getAlternateNames())
		{
			if (StringUtils.isNotBlank(alternateName))
			{
				result.add(alternateName);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringSet lineToNames(final String line)
	{
		StringSet result;

		result = new StringSet();

		String[] tokens = line.split("\t");

		String name = tokens[1];
		if (StringUtils.isNotBlank(name))
		{
			result.add(name);
		}

		String asciiName = tokens[2];
		if (StringUtils.isNotBlank(asciiName))
		{
			result.add(asciiName);
		}

		String[] alternateNames = tokens[3].split(",");

		for (String alternateName : alternateNames)
		{
			if (StringUtils.isNotBlank(alternateName))
			{
				result.add(alternateName);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringSet namesToRawWords(final StringSet source)
	{
		StringSet result;

		result = new StringSet();

		for (String name : source)
		{
			result.addAll(nameToRawWords(name));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static WordTriage namesToWords(final StringSet source)
	{
		WordTriage result;

		result = new WordTriage();

		for (String name : source)
		{
			result.addAll(nameToWords(name));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringSet nameToRawWords(final String source)
	{
		StringSet result;

		result = new StringSet();

		if (source != null)
		{
			String name = source.toLowerCase(Locale.ROOT);

			//
			String[] words = name.split("[" + SEPARATOR_CHARACTERS + "]");

			// logger.debug("source=[{}]=>name=[{}]=>split={}", source, name,
			// words.length);

			for (String word : words)
			{
				// logger.debug("word=[{}]", word);
				if (StringUtils.isNotBlank(word))
				{
					result.add(word);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static WordTriage nameToWords(final String source)
	{
		WordTriage result;

		result = new WordTriage();

		if (source != null)
		{
			String name = source.toLowerCase(Locale.ROOT);

			//
			String[] words = name.split("[" + SEPARATOR_CHARACTERS + "]");

			// logger.debug("source=[{}]=>name=[{}]=>split={}", source, name,
			// words.length);

			for (String word : words)
			{
				// logger.debug("word=[{}]", word);
				if ((StringUtils.isNotBlank(word)) && (word.length() >= 2) && (!startsWithDigit(word)) && (word.getBytes().length <= WORD_MAX_LENGTH))
				{
					result.getSelection().add(word);
				}
				else
				{
					result.getRejection().add(word);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean startsWithDigit(final String source)
	{
		boolean result;

		if (source == null)
		{
			result = false;
		}
		else if (source.length() == 0)
		{
			result = false;
		}
		else if (Character.isDigit(source.charAt(0)))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @throws IOException
	 */
	public static void wordSeeksToWordSeekSeeks(final File source, final File target) throws IOException
	{
		Chronometer chronoStep = new Chronometer();

		long currentSeekValue = 0;
		BufferedReader in = null;
		PrintWriter out = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(target), DEFAULT_CHARSET_NAME));

			int lineCount = 0;
			boolean ended = false;
			while (!ended)
			{
				if (lineCount % 1000000 == 0)
				{
					System.gc();

					logger.debug("\tlineCount=" + lineCount + "\tcurrentSeekValue=" + currentSeekValue + " \tmemory=" + Runtime.getRuntime().totalMemory() / 1024 / 1024 + " Mo \t"
							+ ((int) (chronoStep.stop().interval() / 1000)) + " s");
					chronoStep.reset();
				}

				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					lineCount += 1;

					String word = line.split("\t")[0];

					out.println(StringUtils.rightPad(word + "\t" + currentSeekValue, 11 + WORD_MAX_LENGTH));

					currentSeekValue += line.getBytes().length + 1;
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}
	}
}
