/*
 * Copyright (C) 2015-2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import fr.devinsy.util.StringList;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Feature
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Feature.class);

	private FeatureClass featureClass;
	private String featureCode;
	private String shortDescription;
	private String description;

	/**
	 * 
	 * @param className
	 * @param code
	 */
	public Feature(final String featureClassCode, final String featureCode, final String shortDescription, final String description)
	{
		if (!FeatureClass.isClassCode(featureClassCode))
		{
			throw new IllegalArgumentException("Bad feature class value.");
		}
		else
		{
			this.featureClass = FeatureClass.valueOfCode(featureClassCode);
			this.featureCode = featureCode;
			this.shortDescription = shortDescription;
			this.description = description;
		}
	}

	/**
	 * 
	 */
	public String getCodePath()
	{
		String result;

		result = convertToFeaturePath(this.featureClass.getCode(), this.featureCode);

		//
		return result;
	}

	public String getDescription()
	{
		return this.description;
	}

	/**
	 * Returns the description path of a this feature.
	 * 
	 * @param className
	 * @param featureCode
	 * @return
	 */
	public String getDescriptionPath()
	{
		String result;

		if (this.featureCode == null)
		{
			result = this.shortDescription;
		}
		else
		{
			result = String.format("%s / %s", this.featureClass.getDescription(), this.shortDescription);
		}

		//
		return result;
	}

	public FeatureClass getFeatureClass()
	{
		return this.featureClass;
	}

	/**
	 * 
	 * @return
	 */
	public String getFeatureClassCode()
	{
		String result;

		result = this.featureClass.getCode();

		//
		return result;
	}

	public String getFeatureCode()
	{
		return this.featureCode;
	}

	public String getShortDescription()
	{
		return this.shortDescription;
	}

	public void setCode(final String code)
	{
		this.featureCode = code;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public void setFeatureClass(final FeatureClass featureClass)
	{
		this.featureClass = featureClass;
	}

	public void setShortDescription(final String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public String toString()
	{
		String result;

		StringList buffer = new StringList();

		buffer.append(getCodePath());
		buffer.append(this.shortDescription);
		buffer.append(this.description);

		result = buffer.toStringSeparatedBy("\t");

		//
		return result;
	}

	/**
	 * 
	 * @param className
	 * @param featureCode
	 * @return
	 */
	public static Pair<String, String> convertToCodes(final String codePath)
	{
		Pair<String, String> result;

		if (StringUtils.isBlank(codePath))
		{
			result = null;
		}
		else
		{
			String codes[] = codePath.split("\\.");

			if (codes.length < 2)
			{
				result = Pair.of(codes[0], null);
			}
			else
			{
				result = Pair.of(codes[0], codes[1]);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param featureClassCode
	 * @param featureCode
	 * @return
	 */
	public static String convertToFeaturePath(final String featureClassCode, final String featureCode)
	{
		String result;

		if (featureClassCode == null)
		{
			result = null;
		}
		else
		{
			StringList buffer = new StringList();
			buffer.append(featureClassCode);
			buffer.append(featureCode);

			result = buffer.toStringSeparatedBy(".");
		}

		//
		return result;
	}
}
