/*
 * Copyright (C) 2015-2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.StringList;

/**
 * This class implements an administrative path.
 * 
 * An administrative code path is a concatenation of administrative codes
 * separated by point: CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
 * 
 * Examples of administrative path:
 * <ul>
 * <li>US: United-States</li>
 * <li>US.KY: Kentucky</li>
 * <li>US.KY.017: Bourbon County</li>
 * </ul>
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AdministrativePath
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AdministrativePath.class);

	private String codePath;
	private String toponym;
	private Integer geonameId;

	/**
	 * 
	 * @param className
	 * @param code
	 */
	public AdministrativePath(final String path, final String description, final Integer geonameId)
	{
		if (StringUtils.isBlank(path))
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.codePath = path;
			this.toponym = description;
			this.geonameId = geonameId;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getCode1()
	{
		String result;

		String[] tokens = this.codePath.split("\\.");

		if (tokens.length > 1)
		{
			result = tokens[1];
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getCode2()
	{
		String result;

		String[] tokens = this.codePath.split("\\.");

		if (tokens.length > 2)
		{
			result = tokens[2];
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getCode3()
	{
		String result;

		String[] tokens = this.codePath.split("\\.");

		if (tokens.length > 3)
		{
			result = tokens[3];
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getCode4()
	{
		String result;

		String[] tokens = this.codePath.split("\\.");

		if (tokens.length > 4)
		{
			result = tokens[4];
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getCountyCode()
	{
		String result;

		result = this.codePath.split("\\.")[0];

		//
		return result;
	}

	public int getGeonameId()
	{
		return this.geonameId;
	}

	public String getTag()
	{
		return this.codePath;
	}

	public String getToponym()
	{
		return this.toponym;
	}

	public void setGeonameId(final int geonameId)
	{
		this.geonameId = geonameId;
	}

	public void setTag(final String tag)
	{
		this.codePath = tag;
	}

	public void setToponym(final String toponymValue)
	{
		this.toponym = toponymValue;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public String toString()
	{
		String result;

		StringList buffer = new StringList();

		buffer.append(this.codePath);
		buffer.append(this.geonameId);

		result = buffer.toStringSeparatedBy("\t");

		//
		return result;
	}

	/**
	 * 
	 * @param className
	 * @param code
	 * @return
	 */
	public static String convertToCodePath(final String countryCode, final String code1, final String code2, final String code3, final String code4)
	{
		String result;

		if (countryCode == null)
		{
			result = null;
		}
		else
		{
			StringList buffer = new StringList();
			buffer.append(countryCode);
			buffer.append(code1);
			buffer.append(code2);
			buffer.append(code3);
			buffer.append(code4);

			result = buffer.toStringSeparatedBy(".");
		}

		//
		return result;
	}
}
