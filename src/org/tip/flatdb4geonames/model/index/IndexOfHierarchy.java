/*
 * Copyright (C) 2015-2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class IndexOfHierarchy
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(IndexOfHierarchy.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private Map<Integer, Integer> index;

	/**
	 * @throws IOException
	 * 
	 */
	public IndexOfHierarchy(final File source) throws IOException
	{
		//
		this.index = new HashMap<Integer, Integer>();

		//
		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));

			int lineCount = 0;
			boolean ended = false;
			while (!ended)
			{
				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					// logger.debug(line);
					lineCount += 1;

					String[] tokens = line.split("\t");

					if (tokens.length == 3)
					{
						String parent = tokens[0];
						String child = tokens[1];
						String relation = tokens[2];

						if (StringUtils.equals(relation, "ADM"))
						{
							this.index.put(Integer.valueOf(child), Integer.valueOf(parent));
						}
					}
				}
			}

			logger.debug("count line=" + lineCount);
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.index.clear();
	}

	/**
	 * 
	 * @param featurePath
	 * @return
	 */
	public Integer get(final Integer geonameId)
	{
		Integer result;

		result = this.index.get(geonameId);

		//
		return result;
	}

	/**
	 * 
	 * @param featurePath
	 * @return
	 */
	public Integer get(final String geonameId)
	{
		Integer result;

		result = get(Integer.valueOf(geonameId));

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.index.size();

		//
		return result;
	}
}
