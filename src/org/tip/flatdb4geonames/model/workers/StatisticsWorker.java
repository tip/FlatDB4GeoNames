/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.workers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.GeoNamesFlatDatabase;
import org.tip.flatdb4geonames.model.index.IndexOfWordSeeksBuilder;
import org.tip.flatdb4geonames.util.StringLongPair;
import org.tip.flatdb4geonames.util.StringLongPairs;

import fr.devinsy.util.StringSet;

/**
 * The Class StatisticsWorker.
 */
public class StatisticsWorker
{
	private static Logger logger = LoggerFactory.getLogger(StatisticsWorker.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Census longest words.
	 * 
	 * @param maxLineCount
	 *            the max line count
	 * @return the string long pairs
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringLongPairs censusLongestWords(final int maxLineCount) throws IOException
	{
		StringLongPairs result;

		result = new StringLongPairs();

		//
		String geonamesFile = FlatDB4GeoNames.instance().getRepository() + File.separator + GeoNamesFlatDatabase.GEONAMES_MAIN_FILENAME;

		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(geonamesFile)), DEFAULT_CHARSET_NAME));

			boolean ended = false;
			int lineCount = 0;
			HashMap<String, StringLongPair> indexOfWords = new HashMap<String, StringLongPair>();
			while (!ended)
			{
				String line = in.readLine();
				lineCount += 1;

				if (lineCount % 100000 == 0)
				{
					logger.debug("lineCount={} {}", lineCount, indexOfWords.size());
				}

				if (line == null)
				{
					ended = true;
				}
				else
				{
					StringSet words = IndexOfWordSeeksBuilder.geoNameLineToRawWords(line);

					for (String word : words.toStringList())
					{
						if (word.length() > 25)
						{
							StringLongPair pair = indexOfWords.get(word);

							if (pair == null)
							{
								pair = new StringLongPair(word, word.length());
								pair.inc();
								result.add(pair);
								indexOfWords.put(word, pair);
							}
							else
							{
								pair.inc();
							}
						}
					}
				}

			}

			for (StringLongPair pair : indexOfWords.values())
			{
				result.add(pair);
			}

			result.sortByLong().reverse();

			while (result.size() > maxLineCount)
			{
				indexOfWords.remove(result.removeLast());
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Census longest words 2.
	 * 
	 * @param maxLineCount
	 *            the max line count
	 * @return the string long pairs
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringLongPairs censusLongestWords2(final int maxLineCount) throws IOException
	{
		StringLongPairs result;

		result = new StringLongPairs();

		//
		String geonamesFile = FlatDB4GeoNames.instance().getRepository() + File.separator + GeoNamesFlatDatabase.GEONAMES_MAIN_FILENAME;

		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(geonamesFile)), DEFAULT_CHARSET_NAME));

			boolean ended = false;
			int lineCount = 0;
			HashMap<String, StringLongPair> indexOfWords = new HashMap<String, StringLongPair>();
			while (!ended)
			{
				String line = in.readLine();
				lineCount += 1;

				if (lineCount % 100000 == 0)
				{
					logger.debug("lineCount={}", lineCount);
				}

				if (line == null)
				{
					ended = true;
				}
				else
				{
					StringSet words = IndexOfWordSeeksBuilder.geoNameLineToRawWords(line);

					for (String word : words.toStringList())
					{
						StringLongPair pair = indexOfWords.get(word);

						if (pair == null)
						{
							pair = new StringLongPair(word, word.length());
							pair.inc();
							result.add(pair);
							indexOfWords.put(word, pair);
						}
						else
						{
							pair.inc();
						}
					}

					result.sortByLong().reverse();

					while (result.size() > maxLineCount)
					{
						indexOfWords.remove(result.removeLast());
					}
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Census word matching.
	 * 
	 * @param maxLineCount
	 *            the max line count
	 * @return the string long pairs
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringLongPairs censusWordMatching(final int maxLineCount) throws IOException
	{
		StringLongPairs result;

		result = new StringLongPairs();

		//
		String index2 = FlatDB4GeoNames.instance().getRepository() + File.separator + IndexOfWordSeeksBuilder.INDEX2_FILENAME;
		String index1 = FlatDB4GeoNames.instance().getRepository() + File.separator + IndexOfWordSeeksBuilder.INDEX1_FILENAME;

		BufferedReader in2 = null;
		BufferedReader in1 = null;
		try
		{
			in2 = new BufferedReader(new InputStreamReader(new FileInputStream(new File(index2)), DEFAULT_CHARSET_NAME));
			in1 = new BufferedReader(new InputStreamReader(new FileInputStream(new File(index1)), DEFAULT_CHARSET_NAME));

			boolean ended = false;
			int lineCount = 0;
			while (!ended)
			{
				String line2 = in2.readLine();
				String line1 = in1.readLine();
				lineCount += 1;

				if (lineCount % 100000 == 0)
				{
					logger.debug("lineCount={}", lineCount);
				}

				if (line2 == null)
				{
					ended = true;
				}
				else
				{
					String word = line2.split("\t")[0];

					long matchingCount = 1;
					for (int position = 0; position < line1.length(); position++)
					{
						if (line1.charAt(position) == ',')
						{
							matchingCount += 1;
						}
					}

					StringLongPair pair = new StringLongPair(word, matchingCount);
					result.add(pair);
					result.sortByLong().reverse();

					while (result.size() > maxLineCount)
					{
						result.removeLast();
					}
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in2);
			IOUtils.closeQuietly(in1);
		}

		//
		return result;
	}
}
