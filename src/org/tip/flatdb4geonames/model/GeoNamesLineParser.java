/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.index.FeatureClass;

import fr.devinsy.util.StringList;

/**
 * The Class GeoNamesLineParser.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeoNamesLineParser
{
	private static Logger logger = LoggerFactory.getLogger(GeoNamesLineParser.class);

	/**
	 * Parses the.
	 * 
	 * @param source
	 *            the source
	 * @return the geo names line
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public static GeoNamesLine parse(final String source) throws FlatDB4GeoNamesException
	{
		GeoNamesLine result;

		result = new GeoNamesLine();

		String[] tokens = source.split("\t");

		if (!NumberUtils.isDigits(tokens[0]))
		{
			throw new FlatDB4GeoNamesException("Bad line format.");
		}
		else
		{
			// 0 geonameid : integer id of record in geonames database
			result.setGeoNameId(Long.parseLong(tokens[0]));

			// 1 name : name of geographical point (utf8) varchar(200)
			result.setName(tokens[1]);

			// 2 asciiname : name of geographical point in plain ascii
			// characters, varchar(200)
			result.setAsciiName(tokens[2]);

			// 3 alternatenames : alternatenames, comma separated, ascii names
			// automatically transliterated, convenience attribute from
			// alternatename table, varchar(10000)
			for (String alternateName : tokens[3].split(","))
			{
				if (StringUtils.isNotBlank(alternateName))
				{
					result.getAlternateNames().add(alternateName);
				}
			}

			//
			if (StringUtils.isNotBlank(tokens[4]))
			{
				// 4 latitude : latitude in decimal degrees (wgs84)
				result.setLatitude(Double.parseDouble(tokens[4]));

				// 5 longitude : longitude in decimal degrees (wgs84)
				result.setLongitude(Double.parseDouble(tokens[5]));
			}

			// 6 feature class : see http://www.geonames.org/export/codes.html,
			// char(1)
			result.setFeatureClass(FeatureClass.valueOfCode(tokens[6]));

			// 7 feature code : see http://www.geonames.org/export/codes.html,
			// varchar(10)
			result.setFeatureCode(tokens[7]);

			// 8 country code : ISO-3166 2-letter country code, 2 characters
			result.setCountryCode(tokens[8]);

			// 9 cc2 : alternate country codes, comma separated, ISO-3166
			// 2-letter country code, 60 characters
			for (String alternateCountryCode : tokens[9].split(","))
			{
				if (StringUtils.isNotBlank(alternateCountryCode))
				{
					result.getAlternateCountryCodes().add(alternateCountryCode);
				}
			}

			// 10 admin1 code : fipscode (subject to change to iso code), see
			// exceptions below, see file admin1Codes.txt for display names of
			// this code; varchar(20)
			if (StringUtils.isNotBlank(tokens[10]))
			{
				result.setAdminCode1(tokens[10]);
			}

			// 11 admin2 code : code for the second administrative division, a
			// county in the US, see file admin2Codes.txt; varchar(80)
			if (StringUtils.isNotBlank(tokens[11]))
			{
				result.setAdminCode2(tokens[11]);
			}

			// 12 admin3 code : code for third level administrative division,
			// varchar(20)
			if (StringUtils.isNotBlank(tokens[12]))
			{
				result.setAdminCode3(tokens[12]);
			}

			// 13 admin4 code : code for fourth level administrative division,
			// varchar(20)
			if (StringUtils.isNotBlank(tokens[13]))
			{
				result.setAdminCode4(tokens[13]);
			}

			// 14 population : bigint (8 byte int)
			if (StringUtils.isNotBlank(tokens[14]))
			{
				result.setPopulation(Long.parseLong(tokens[14]));
			}

			// 15 elevation : in meters, integer
			if (StringUtils.isNotBlank(tokens[15]))
			{
				result.setElevation(Integer.parseInt(tokens[15]));
			}

			// 16 dem : digital elevation model, srtm3 or gtopo30, average
			// elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m)
			// area in meters, integer. srtm processed by cgiar/ciat.
			if (StringUtils.isNotBlank(tokens[16]))
			{
				result.setElevation(Integer.parseInt(tokens[16]));
			}

			// 17 timezone : the timezone id (see file timeZone.txt) varchar(40)
			result.setTimeZone(tokens[17]);

			// 18 modification date : date of last modification in yyyy-MM-dd
			// format
			result.setModificationDate(tokens[18]);
		}

		//
		return result;
	}

	/**
	 * Parses the.
	 * 
	 * @param lines
	 *            the lines
	 * @return the geo names lines
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public static GeoNamesLines parse(final StringList lines) throws FlatDB4GeoNamesException
	{
		GeoNamesLines result;

		if (lines == null)
		{
			result = new GeoNamesLines();
		}
		else
		{
			result = new GeoNamesLines(lines.size());

			for (String line : lines)
			{
				result.add(parse(line));
			}
		}

		//
		return result;
	}
}
