/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import java.util.ArrayList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.index.FeatureClass;
import org.tip.flatdb4geonames.model.index.FeatureClasses;

/**
 * The Class GeoNamesLines.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeoNamesLines extends ArrayList<GeoNamesLine>
{
	private static final long serialVersionUID = 8370145125552994190L;

	private static Logger logger = LoggerFactory.getLogger(GeoNamesLines.class);

	/**
	 * Instantiates a new geo names lines.
	 */
	public GeoNamesLines()
	{
		super();
	}

	/**
	 * Instantiates a new geo names lines.
	 * 
	 * @param capacity
	 *            the capacity
	 */
	public GeoNamesLines(final int capacity)
	{
		super(capacity);
	}

	/**
	 * Contains geo name id.
	 * 
	 * @param geoNameId
	 *            the geo name id
	 * @return true, if successful
	 */
	public boolean containsGeoNameId(final long geoNameId)
	{
		boolean result;

		if (getByGeoNameId(geoNameId) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Gets the by feature class.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the by feature class
	 */
	public GeoNamesLines getByFeatureClass(final FeatureClass criteria)
	{
		GeoNamesLines result;

		result = new GeoNamesLines();

		for (GeoNamesLine toponym : this)
		{
			if (toponym.getFeatureClass() == criteria)
			{
				result.add(toponym);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the by feature classes.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the by feature classes
	 */
	public GeoNamesLines getByFeatureClasses(final FeatureClasses criteria)
	{
		GeoNamesLines result;

		result = new GeoNamesLines();

		for (GeoNamesLine toponym : this)
		{
			if (criteria.contains(toponym.getFeatureClass()))
			{
				result.add(toponym);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the by geo name id.
	 * 
	 * @param geoNameId
	 *            the geo name id
	 * @return the by geo name id
	 */
	public GeoNamesLine getByGeoNameId(final long geoNameId)
	{
		GeoNamesLine result;

		boolean ended = false;
		Iterator<GeoNamesLine> iterator = this.iterator();
		result = null;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				GeoNamesLine line = iterator.next();

				if (line.getGeoNameId() == geoNameId)
				{
					ended = true;
					result = line;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}
}
