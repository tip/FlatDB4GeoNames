/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

/**
 * The Class GeoNamesSearchResult.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeoNamesSearchResult
{
	private GeoNamesSearchCriteria criteria;
	private long duration;
	private GeoNamesLines lines;

	/**
	 * Instantiates a new geo names search result.
	 * 
	 * @param criteria
	 *            the criteria
	 * @param lines
	 *            the lines
	 * @param duration
	 *            the duration
	 */
	public GeoNamesSearchResult(final GeoNamesSearchCriteria criteria, final GeoNamesLines lines, final long duration)
	{
		this.criteria = criteria;
		this.lines = lines;
		this.duration = duration;
	}

	/**
	 * Gets the criteria.
	 * 
	 * @return the criteria
	 */
	public GeoNamesSearchCriteria getCriteria()
	{
		return this.criteria;
	}

	/**
	 * Gets the duration.
	 * 
	 * @return the duration
	 */
	public long getDuration()
	{
		return this.duration;
	}

	/**
	 * Gets the lines.
	 * 
	 * @return the lines
	 */
	public GeoNamesLines getLines()
	{
		return this.lines;
	}
}
