/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.cli;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesFactory;
import org.tip.flatdb4geonames.model.GeoNamesFlatDatabase;
import org.tip.flatdb4geonames.model.GeoNamesLine;
import org.tip.flatdb4geonames.model.GeoNamesLines;
import org.tip.flatdb4geonames.util.BuildInformation;
import org.tip.flatdb4geonames.util.ToolBox;

import fr.devinsy.util.StringList;

/**
 * The Class FlatDB4GeoNamesCLI.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class FlatDB4GeoNamesCLI
{
	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNamesCLI.class);

	/**
	 * Println.
	 * 
	 * @param results
	 *            the results
	 */
	public static void println(final GeoNamesLines results)
	{
		for (GeoNamesLine result : results)
		{
			System.out.println("-------------------------------------------");
			System.out.println(result.toLine());
		}
	}

	/**
	 * Run.
	 * 
	 * @param args
	 *            the args
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	public static void run(final String[] args) throws IOException, FlatDB4GeoNamesException
	{
		//
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
		{
			/**
             * 
             */
			@Override
			public void uncaughtException(final Thread thread, final Throwable exception)
			{
				if (exception instanceof OutOfMemoryError)
				{
					System.err.println("Java ran out of memory! Increase it.");
					logger.error(exception.getMessage(), exception);
				}
				else if (exception instanceof FlatDB4GeoNamesException)
				{
					System.err.println(exception.getMessage());
					logger.error(exception.getMessage(), exception);
				}
				else
				{
					System.err.println("A system error occured: " + exception.getMessage());
					System.err.println("Please, call developers.");
					logger.error(exception.getMessage(), exception);
				}
			}
		});

		logger.debug("args={}{}", args.length, StringList.toStringWithBrackets(args));

		// This following code has high cyclomatic because it is implementing a
		// transducer of the parameters.
		if (args.length == 0)
		{
			showHelp();
		}
		else
		{
			if (ToolBox.matchAny(args[0], "-h", "-help", "--help"))
			{
				if (args.length != 1)
				{
					System.err.println("Bad parameters: " + StringList.toStringWithBrackets(args));
				}
				showHelp();
			}
			else if (StringUtils.equals(args[0], "download"))
			{
				if (args.length == 1)
				{
					FlatDB4GeoNamesFactory.downloadGeoNamesFiles(new File(System.getProperty("user.dir")));
				}
				else if (args.length == 2)
				{
					FlatDB4GeoNamesFactory.downloadGeoNamesFiles(ToolBox.normalizePath(args[1]));
				}
				else
				{
					System.err.println("Bad parameters: " + StringList.toStringWithBrackets(args));
					showHelp();
				}
			}
			else if (StringUtils.equals(args[0], "buildIndex"))
			{
				if (args.length == 1)
				{
					FlatDB4GeoNamesFactory.buildIndex(new File(System.getProperty("user.dir")));
				}
				else if (args.length == 2)
				{
					FlatDB4GeoNamesFactory.buildIndex(ToolBox.normalizePath(args[1]));
				}
				else
				{
					System.err.println("Bad parameters: " + StringList.toStringWithBrackets(args));
					showHelp();
				}
			}
			else if (StringUtils.equals(args[0], "create"))
			{
				if (args.length == 1)
				{
					FlatDB4GeoNamesFactory.create(new File(System.getProperty("user.dir")));
				}
				else if (args.length == 2)
				{
					FlatDB4GeoNamesFactory.create(ToolBox.normalizePath(args[1]));
				}
				else
				{
					System.err.println("Bad parameters: " + StringList.toStringWithBrackets(args));
					showHelp();
				}
			}
			else if (StringUtils.equals(args[0], "search"))
			{
				if (args.length == 2)
				{
					GeoNamesFlatDatabase database = FlatDB4GeoNamesFactory.open(new File(System.getProperty("user.dir")));
					GeoNamesLines results = database.search(args[1]);
					println(results);
					database.close();
				}
				else if (args.length == 3)
				{
					GeoNamesFlatDatabase database = FlatDB4GeoNamesFactory.open(ToolBox.normalizePath(args[1]));
					GeoNamesLines results = database.search(args[2]);
					println(results);
					database.close();
				}
				else
				{
					System.err.println("Bad parameters: " + StringList.toStringWithBrackets(args));
					showHelp();
				}
			}
			else
			{
				System.err.println("Bad parameters: " + StringList.toStringWithBrackets(args));
				showHelp();
			}
		}
	}

	/**
	 * Show help.
	 */
	public static void showHelp()
	{
		StringList message = new StringList();
		message.append("FlatDB4GeoNames, version ").append(new BuildInformation().version());
		message.append(", JVM ").append(System.getProperty("java.version"));
		message.append(", ").append(System.getProperty("os.name")).append(" ").append(System.getProperty("os.version")).append(" ").append(System.getProperty("sun.arch.data.model")).appendln("bits");
		message.appendln();
		message.appendln("FlatDB4GeoNames provides an easy, local and fast Java access to GeoNames data.");
		message.appendln();
		message.appendln("Usage:");
		message.appendln("        flatdb4geonames                                  run FlatDB4GeoNames GUI");
		message.appendln("        flatdb4geonames [ -h | -help | --help ]          display this help message");
		message.appendln("        flatdb4geonames download [directory]             download GeoNames files");
		message.appendln("        flatdb4geonames buildIndex [directory]           build FlatDB4GeoNames indexes");
		message.appendln("        flatdb4geonames create [directory]               download + buildIndex");
		message.appendln("        flatdb4geonames search [directory] input         search input in FlatDB4GeoNames database");
		message.appendln();
		message.appendln("If directory is not set, the current directory is defined as the FlatDB4GeoNames database location.");

		System.out.println(message.toString());
	}
}
